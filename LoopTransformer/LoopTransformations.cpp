#include "LoopTransformations.h"
#include <sstream>
#include <memory>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include "LoopSplitter.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace TfUtils;

using namespace LoopTransformations;

namespace {

/// A class that attempts to move statements out
/// from *loop* body.
class StmtMover {
	std::size_t moved{0};
	STRS bringout_vars;
	const bool bringout_everything;
	SgForStatement *loop;
	const bool log;

	bool movable(const SgInitializedName *var_name,
		const SgExpression *rhs, const bool declaration);

	void bringout_statement(const string &what, const string &name,
				SgStatement *s)
	{
		l << what << name << " at " << s;
		l << " will be moved from " << loop << endl;
		/// \todo handle symbol tables
		removeStatement(s);
		insertStatementBefore(loop, s);
		moved++;
	}

	void handle_expr_stmt(SgExprStatement *s)
	{
		auto e = isSgAssignOp(s->get_expression());
		if (!e)
			return;
		auto lhs = e->get_lhs_operand_i();
		auto rhs = e->get_rhs_operand_i();
		auto var_name = get_var_name(lhs);
		if (!bringout_everything && !bringout_vars.count(var_name))
			return;

		auto var_iname = get_var_initialized_name(lhs);
		if (movable(var_iname, rhs, false))
			bringout_statement("Assignment to ", var_name, s);
	}

	void handle_aggr(const SgInitializedName *v, SgStatement *s,
				bool move)
	{
		if (move)
			bringout_statement("Initialization of ",
					get_var_name(v), s);
		else
			bringout_vars.erase(v->get_name());
	}

	/// \todo when erasing var from the set and when it
	/// prevents us from moving something after, does it
	/// work when the statement order is reversed?
	void handle_declaration(SgVariableDeclaration *d)
	{
		auto variables = d->get_variables();
		// each declaration contains only one variable
		assert(variables.size() == 1);
		auto v = variables[0];
		if (!bringout_everything && !bringout_vars.count(v->get_name()))
			return;

		auto gen_init = v->get_initializer();
		if (auto assign_init = isSgAssignInitializer(gen_init)) {
			auto ivar = assign_init->get_operand();
			handle_aggr(v, d, movable(v, ivar, true));
		} else if (auto ag_init = isSgAggregateInitializer(gen_init)) {
			auto inits = ag_init->get_initializers()
						->get_expressions();
			auto possible = true;
			for (auto i : inits) {
				possible = possible && movable(v, i, true);
				if (!possible)
					break;
			}
			handle_aggr(v, d, possible);
		} else if (gen_init) {
			l << LOG_WARN << d;
			l << "Unsupported initializer of " << v << endl;
		}
	}
public:
	StmtMover(const STRS &vars, bool everything, SgForStatement *loop)
		: bringout_vars(vars), bringout_everything(everything),
		loop(loop), log(!bringout_everything)
	{ }

	std::size_t get_moved() { return moved; }

	void operator()(SgStatement *s)
	{
		if (auto expr_stmt = isSgExprStatement(s)) {
			handle_expr_stmt(expr_stmt);
		} else if (auto decl = isSgVariableDeclaration(s)) {
			handle_declaration(decl);
		}
	}
};

/// Checks if
/// * *var_name* or anything it depends on isn't modified
///   anywhere in loop body
/// * *rhs* does not contain any loop indices or anything
///   depending on them
/// * or modifiers or references to *var_name*
/// * if that is a declaration, that there are no variables
///   with the same name in outer scope
bool StmtMover::movable(const SgInitializedName *var_name,
		const SgExpression *rhs, const bool declaration)
{
	auto decl = static_cast<string>(declaration ?
			"the declaration of " : "the assignment to ");
	auto announce_error = [&] (const string &reason) {
		if (!log)
			return;
		l << LOG_ERR << rhs << " Unable to move ";
		l << decl << var_name;
		l << " out from the loop because " << reason << endl;
	};

	CNS loop_indices;
	get_nested_loops_indices(loop, loop_indices);
	auto loop_body = get_nested_loops_body(loop);
	auto loop_body_block = isSgBasicBlock(loop_body);
	assert(loop_body_block);

	auto refs_array = get_dependant_variables(loop_body_block,
						var_name);
	auto dependant_vars = get<1>(refs_array[0]);

	// Check dependency on indices
	auto index_location = find_if(dependant_vars.begin(),
					dependant_vars.end(),
					RefFinder(&loop_indices));
	if (index_location != dependant_vars.end()) {
		announce_error("it depends on loop indices");
		return false;
	}
	if (expr_contains_any_of_vars(rhs, var_name)) {
		announce_error("it is assigned to itself");
		return false;
	}

	// Check modifier presence
	NV local_modified;
	get_modified_variables(rhs, local_modified);
	if (local_modified.size()) {
		announce_error("it contains modifiers");
		return false;
	}

	// Check the var's const-ness
	CNV global_modified;
	get_modified_variables(loop_body_block, global_modified);
	// one occurence of var_name is the one
	// we are removing in case of a usual assignment,
	// more of them means trouble
	if (count(global_modified.begin(), global_modified.end(),
				var_name) > (declaration ? 0 : 1)) {
		announce_error("it is modified in the loop body");
		return false;
	}

	CNS local_referenced;
	get_referenced_vars(rhs, local_referenced);
	auto modpos = find_if(local_referenced.begin(),
				local_referenced.end(),
		[&global_modified] (const SgInitializedName *r) {
			return std::find(global_modified.begin(),
					global_modified.end(), r)
				!= global_modified.end();
			});
	if (modpos != local_referenced.end()) {
		stringstream m;
		m << *modpos;
		m << ", which it depends on, is modified in the loop body";
		announce_error(m.str());
		return false;
	}

	// Check outer scope
	if (declaration) {
		auto enc_scope =
			getEnclosingNode<SgScopeStatement>(loop_body_block);
		auto other_declaration =
			lookupVariableSymbolInParentScopes(var_name->get_name(),
								enc_scope);
		if (other_declaration) {
			announce_error("enclosing scope contains a variable with the same name");
			return false;
		}
	}
	return true;
}

/// Iterates through top-level statements of *loop* body
/// and brings them to the outer scope if possible.
///
/// Returns whether any statements were brought out.
///
/// *bringout_vars* are passed by value because when a variable
/// is considered unremovable at some time, it is prevented from
/// being removed at further occasions.
std::size_t move_from_loop(SgForStatement *loop, bool br_e,
					const STRS &br_v)
{
	assert(loop);
	auto body = get_nested_loops_main_block(loop);
	auto stmts = body->get_statements();
	return boost::for_each(stmts, StmtMover(br_v, br_e, loop)).get_moved();
}
} // namespace

/// For loop at *block* or all loops inside *block*
/// move:
/// * all invariant statements, if *all* == true
/// * statements modifying a variable form *vars* otherwise
///
/// out of the loop body and insert them before the loop.
void LoopTransformations::bringout_statements(SgForStatement *block,
					bool all, STRS&& vars)
{
	if (auto loop = isSgForStatement(block)) {
		auto moved = move_from_loop(loop, all, vars);
		l << LOG_INFO << "Moved " << moved;
		l << " statements from the loop at " << block << endl;
	} else if (isSgScopeStatement(block)) {
		auto loops = querySubTree<SgForStatement>(block);
		for (auto it = loops.rbegin(); it != loops.rend(); it++)
			move_from_loop(*it, all, vars);
	}
}

namespace {
/// Assuming *top_mod_vars* are numbered from 0 to n-1,
/// puts their numbers into *result* in order corresponding to
/// their positions in *p.permutation_indices*
bool get_permutation_order(const vector<string> &permutate,
				const CNV &top_mod_vars,
				vector<size_t> &result)
{
	typedef map<string, size_t> IndexT;
	size_t indices_n = permutate.size();
	assert(indices_n == top_mod_vars.size());
	result.reserve(indices_n);
	IndexT indices;
	//transform(top_mod_vars.begin(), top_mod_vars.end(),
	//                inserter(indices, indices.end()),
	//                [IndexT] (const SgInitializedName *v) {
	//                        IndexT::value_type(v->get_name(), i);
	//                });
	for (size_t i = 0; i < indices_n; i++) {
		auto e = IndexT::value_type(top_mod_vars[i]->get_name(), i);
		indices.insert(e);
	}
	for (auto v : permutate) {
		auto index = indices.find(v);
		if (index == indices.end())
			return false;
		result.push_back(index->second);
	}
	return true;
}
} // namespace

/// An interface to ROSE procedure that interchanges some perfectly nested loops
///
void LoopTransformations::permutate_loops(SgForStatement *loop,
			std::vector<std::string>&& permutation_indices)
{
	assert(loop);
	if (!isCanonicalForLoop(loop))
		throw Exception("Permutation is supported only for canonical 'for' loops",
				loop);
	l << "Permutating nested loops at " << loop << endl;
	CNV indices;
	get_nested_loops_indices(loop, indices);
	if (indices.size() != permutation_indices.size())
		throw Exception("number of variables (= "
				+ to_string(permutation_indices.size())
				+ ") provided as parameters to permutation "
				"does not match the number of loop indices (= "
				+ to_string(indices.size()), loop);
	vector<size_t> order;
	if (!get_permutation_order(permutation_indices, indices, order))
		throw Exception("could not find some of the indices provided as loop permutation parameters",
				loop);
	loop_interchange(loop, permutation_indices.size(), order);
}

namespace {
/// Helper class for shift_indices().
class IndexShifter: public unary_function<SgForStatement*, void> {
	typedef std::multimap<std::string, int> ShiftParams;
	ShiftParams p;
public:
	IndexShifter(ShiftParams&& p): p(std::move(p)) {}
	void operator()(SgForStatement *loop) {
		SgInitializedName* ivar = nullptr;
		SgExpression* lb = nullptr;
		SgExpression* ub = nullptr;
		SgExpression* step = nullptr;
		SgStatement* body = nullptr;
		if (!isCanonicalForLoop(loop, &ivar, &lb, &ub, &step, &body))
			return;
		auto shift_pos = p.find(ivar->get_name());
		if (shift_pos == p.end())
			return;
		auto shift = shift_pos->second;
		assert(lb && ub && ivar && body);
		replaceExpression(lb, buildAddOp(deepCopy(lb),
						buildIntVal(shift)));
		replaceExpression(ub, buildAddOp(deepCopy(ub),
						buildIntVal(shift)));
		/// \todo delete lb, ub?

		vector<SgVarRefExp*> refs;
		query_subtree<SgVarRefExp>(body, refs);
		refs.erase(remove_if(refs.begin(), refs.end(),
			[ivar] (SgVarRefExp *ref) {
				return ivar != get_var_initialized_name(ref);
			}),
			refs.end());
		for (auto v : refs)
			replaceExpression(v, buildSubtractOp(deepCopy(v),
						buildIntVal(shift)));
	}
};
} // namespace

/// Handles canonical for loops with indices
/// found in *index_shift_vars* replacing upper and lower bounds
/// of those loops with b+shift
/// and all references to their indices with r-shift
void LoopTransformations::shift_indices(SgForStatement *loop,
		std::multimap<std::string, int>&& index_shift_vars)
{
	assert(loop);
	auto all_loops = get_all_nested_loops(loop);
	boost::for_each(all_loops, IndexShifter(std::move(index_shift_vars)));
}

void LoopTransformations::split_loop(SgForStatement *loop, STRS&& split)
{
	assert(loop);
	LoopSplitter splitter(loop, std::move(split));
	splitter.split_loop();
}

namespace {
/// Returns false if *b1* depends on *b0*, true otherwise.
///
/// Gets variables referenced in one body. For each of them:
/// 0\1   |use |reset|update
/// ------|----|-----|------
/// use   |ok  | fail| fail
/// reset |fail| fail| fail
/// update|fail| fail| ok
/// \todo is this check correct???
bool blocks_are_independent(const SgStatement *b0, const SgStatement *b1)
{
	CNS referenced_in_b0;
	CNS referenced_in_b1;
	CNS referenced_in_both;
	CNS readonly_in_b0;
	CNS readonly_in_b1;
	CNS updated_in_b0;
	CNS updated_in_b1;
	CNS upd_only_in_b0;
	CNS upd_only_in_b1;
	get_referenced_vars(b0, referenced_in_b0);
	get_referenced_vars(b1, referenced_in_b1);
	get_readonly_vars(b0, readonly_in_b0);
	get_readonly_vars(b1, readonly_in_b1);
	get_modified_variables(b0, updated_in_b0, true);
	get_modified_variables(b1, updated_in_b1, true);
	boost::set_intersection(referenced_in_b0, referenced_in_b1,
			inserter(referenced_in_both, referenced_in_both.end()));

	return boost::find_if(referenced_in_both,
		[&](const SgInitializedName *v) {
			return !((readonly_in_b0.count(v)
					&& readonly_in_b1.count(v)) ||
				(updated_in_b0.count(v)
					&& updated_in_b1.count(v)));
		}) == referenced_in_both.end();
}

class LoopSwapper {
	SgBasicBlock *block;
	SgStatementPtrList stmts;

	void err_no_block(int i)
	{
		throw Exception(
			"Unable to swap statements: could not find statement with tag "
			+ to_string(i)
			+ " in the supplied block", block);
	}

	void err_depends(SgStatement *l0, SgStatement *l1)
	{
		stringstream message;
		message << "Unable to swap statements because ";
		message << l0 << " depends on " << l1;
		throw Exception(message.str());
	}

	/// Swap possibility check.
	/// Throws if the check fails.
	void perform_check(SgForStatement *l0, SgForStatement *l1)
	{
		auto b0 = get_nested_loops_main_block(l0);
		auto b1 = get_nested_loops_main_block(l1);
		perform_check(b0, b1);
	}

	/// \todo move the adjacency check to operator()
	void perform_check(SgStatement *s0, SgStatement *s1)
	{
		if (getNextStatement(s0) == s1) {
			if (!blocks_are_independent(s0, s1))
				err_depends(s1, s0);
		} else if (getNextStatement(s1) == s0) {
			if (!blocks_are_independent(s1, s0))
				err_depends(s0, s1);
		} else {
			throw Exception(
				"Unable to swap statements that are not neighbours",
				s0, s1);
		}
	}
public:
	LoopSwapper(SgBasicBlock *b, SgStatementPtrList&& all):
		block(b), stmts(std::move(all)) {};

	void operator()(SWPT i)
	{
		auto i0 = get<0>(i);
		auto i1 = get<1>(i);
		auto check = get<2>(i);
		auto b0_p = boost::find_if(stmts, AttributeFinder(i0));
		auto l1_p = boost::find_if(stmts, AttributeFinder(i1));
		if (b0_p == stmts.end())
			err_no_block(i0);
		if (l1_p == stmts.end())
			err_no_block(i1);
		auto b0 = *b0_p;
		auto b1 = *l1_p;
		if (check) {
			perform_check(b0, b1);
		} else {
			l << LOG_WARN << "You have requested to swap blocks without checking dependencies between them. This may lead to incorrect code generation.";
			l << endl;
		}
		l << "Swapping statements at " << b0 << " and " << b1 << endl;
		using pptr = std::unique_ptr<SgPragmaDeclaration,
						void (*)(SgNode*)>;
		pptr placeholder(buildPragmaDeclaration("tmp"), deepDelete);
		replaceStatement(b0, placeholder.get());
		replaceStatement(b1, b0);
		replaceStatement(placeholder.get(), b1);
	}
};
} // namespace

/// Swaps statements with indices supplied in p.swap_indices.
///
/// The swapped blocks should be nearby statements.
/// Blocks are swapped in order, so to swap loops 0 and 2 in
/// the following block, one whould write:
/// \code
/// #pragma x swap(0:1, 0:2, 1:2)
/// {
/// #pragma id(0)
/// for (;;) { ; }
///
/// #pragma id(1)
/// for (;;) { ; }
///
/// #pragma id(2)
/// for (;;) { ; }
/// }
/// \endcode
///
/// For each element from swap_indices:
/// * tries to find statments with attributes containing one of the ids.
/// * if both are found and third tuple element is true, checks whether
///   the second one doesn't depend on anything changed in the first one.
/// * if check succeeds, swaps loops using a placeholder pragma.
void LoopTransformations::swap_blocks(SgBasicBlock *block,
				std::vector<SWPT>&& swap_indices)
{
	assert(block);
	auto all_loops = block->get_statements();
	boost::for_each(swap_indices, LoopSwapper(block, std::move(all_loops)));
}

namespace {
class LoopMerger {
	SgBasicBlock *block;
	vector<SgForStatement*> all_loops;

	void err_no_loop(int i)
	{
		throw Exception(
			"unable to merge loops: could not find loop with tag "
			+ to_string(i) + " in the supplied block", block);
	}

	/// Merges two loops with equal heads
	///
	/// Checks if two loops are equal nests of for loops,
	/// moves statements from the body of the second one
	/// to the body of the first one, removes l1.
	void merge_two_loops(SgForStatement *l0, SgForStatement *l1)
	{
		assert(l0);
		assert(l1);
		if ((getNextStatement(l0) != l1) &&
				(getNextStatement(l1) != l0))
			throw Exception("unable to swap loops that are not neighbours",
					l0);
		auto all_loops0 = get_all_nested_loops(l0);
		auto all_loops1 = get_all_nested_loops(l1);
		/// \todo move get_merge_head_depth() to this file
		auto equality_depth = get_merge_head_depth(all_loops0, all_loops1);
		if (!equality_depth)
			throw Exception("unable to merge loops as they have different heads", l0, l1);
		l << "Merging loops at " << l0 << " and " << l1 << endl;
		auto b0 = get_nested_loops_main_block(l0, equality_depth);
		auto b1 = get_nested_loops_main_block(l1, equality_depth);
		moveStatementsBetweenBlocks(b1, b0);
		removeStatement(l1);
		// deepDelete(l1);
	}
public:
	LoopMerger(SgBasicBlock *b, vector<SgForStatement*>&& all):
		block(b), all_loops(std::move(all)) {};
	void operator()(std::vector<std::size_t> i)
	{
		if (i.size() < 2)
			return;
		auto i0 = i[0];
		auto l0_p = find_if(all_loops.begin(), all_loops.end(),
						AttributeFinder(i0));
		if (l0_p == all_loops.end())
			err_no_loop(i0);
		auto l0 = *l0_p;
		for (auto i1 = i.begin() + 1; i1 != i.end(); i1++) {
			auto l1_p = find_if(all_loops.begin(), all_loops.end(),
						AttributeFinder(*i1));
			if (l1_p == all_loops.end())
				err_no_loop(*i1);
			auto l1 = *l1_p;
			if (l0->get_parent() != l1->get_parent())
				throw Exception("unable to merge loops that have different parents", l0);
			merge_two_loops(l0, l1);
		}
	}
};
} // namespace

/// Merges all sets of loops found in *p.merge_indices*
///
void LoopTransformations::merge_loops(SgBasicBlock *block,
		std::vector<std::vector<std::size_t>>&& merge_indices)
{
	assert(block);
	auto all_loops = querySubTree<SgForStatement>(block);
	for_each(merge_indices.begin(), merge_indices.end(),
			LoopMerger(block, std::move(all_loops)));
}

namespace {
/// Removes statements having numbers [lb, ub) from block.
void remove_stmts_other_than(SgBasicBlock *block, unsigned lb, unsigned ub)
{
	if (lb == ub)
		return;
	auto statements = block->get_statements();
	for (unsigned i = 0; i != statements.size(); i++) {
		if ((i >= lb) && (i < ub))
			continue;
		auto stmt = statements[i];
		removeStatement(stmt);
		// deepDelete(stmt);
	}
}
}

/// Partitions the loop according to pragmas in its body.
/// Currently copies it the required number of times and
/// removes extra statements.
///
/// \todo implement a custom loop copier so that no
/// memory leaks and extra copies would occur.
void LoopTransformations::partition_loop(SgForStatement *loop)
{
	assert(loop);
	l << "partitioning the loop at " << loop << endl;
	auto body = get_nested_loops_main_block(loop);
	auto statements = body->get_statements();

	unsigned partition_beginning = 0;
	for (unsigned i = 0; i != statements.size(); i++) {
		auto stmt = statements[i];
		if (!stmt->attributeExists(BORDER_ATTRIBUTE))
			continue;
		if (i == partition_beginning) {
			l << LOG_WARN;
			l << "you have supplied a partition of size 0" << endl;
			continue;
		}
		SgStatement *copy = deepCopy(loop);
		insertStatementBefore(loop, copy);
		auto block = get_nested_loops_main_block(copy);
		remove_stmts_other_than(block, partition_beginning, i);
		partition_beginning = i;
	}
	remove_stmts_other_than(body, partition_beginning, statements.size());
}


/* Jacobi-1 program */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define L 200000000
#define ITMAX 10
int i;
int it;
double eps;
double MAXEPS = 0.05;
FILE *f;

int main(int an,char **as)
{
/* 1D arrays block distributed along 1 dimension */
  double *A = (malloc(200000000 * sizeof(double )));
  double *B = (malloc(200000000 * sizeof(double )));
/* 1D parallel loop with base array A */

#pragma omp parallel for private(i)
  for (i = 0; i < 200000000; i++) {
    A[i] = 0;
    if (i == 0 || i == 199999999)
      B[i] = 0;
     else
      B[i] = (2 + i);
  }
/* iteration loop */
  for (it = 1; it <= 10; it++) {
    eps = 0;
/* Parallel loop with base array A */
/* calculating maximum in variable eps */

#pragma omp parallel for reduction(max:eps) private(i)
    for (i = 1; i < 199999999; i++) {
      double tmp = fabs(B[i] - A[i]);
      eps = (tmp > eps?tmp : eps);
      A[i] = B[i];
    }
/* Parallel loop with base array B and */
/* with prior updating shadow elements of array A */

#pragma omp parallel for private(i)
    for (i = 1; i < 199999999; i++) {
      B[i] = (A[i - 1] + A[i + 1]) / 2.0;
    }
    printf("it=%4i   eps=%e\n",it,eps);
    if (eps < MAXEPS)
      break;
  }
  f = fopen("/dev/null","wb");
  fwrite(B,sizeof(double ),200000000,f);
  fclose(f);
  return 0;
}

#include <vector>
#include <string>
#include <map>
#include "sage3basic.h"
#include "util.h"

/// The namespace that contains loop-specific transformations.
///
namespace LoopTransformations {
void bringout_statements(SgForStatement *loop,
			bool bringout_everything,
			TfUtils::STRS&& bringout_vars);

void permutate_loops(SgForStatement *loop,
		std::vector<std::string>&& permutation_indices);
void shift_indices(SgForStatement *loop,
		std::multimap<std::string, int>&& index_shift_vars);
void split_loop(SgForStatement *loop, TfUtils::STRS&& split);
void partition_loop(SgForStatement *loop);

void swap_blocks(SgBasicBlock *containing_block,
	std::vector<TfUtils::SWPT>&& swap_indices);
void merge_loops(SgBasicBlock *block,
		std::vector<std::vector<std::size_t>>&& merge_indices);
} // namespace LoopTransformations

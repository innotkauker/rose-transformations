#!/usr/bin/env ruby

prev_header = ''
prev_line = ''
ARGF.each_with_index do |line, i|
	if line =~ /Dependency.*/
		prev_header = line
		puts line
	elsif prev_line =~ /Dependency.*/
		puts line
	else
		puts prev_header
		puts line
	end
	prev_line = line
end


#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t, t1, t2;
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t)
		for (unsigned j = 0; j < 4; j++) {
			t = 15;
			t = 16;
			if (i)
				t = 0;
			if (j)
				a[i][j] = t;
			else
				a[i][j] = i - j;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

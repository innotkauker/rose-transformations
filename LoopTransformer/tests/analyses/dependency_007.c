#include <stdio.h>

// Check more complex loop nestings.

#define n 4
#define m 4

int main()
{
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,6,7,8};
	int cc[] = {1,2,3,4,5,6,6,7,8};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	#pragma a dependency
	{
	for (unsigned iter = 0; iter < bk; iter++) {
		for (unsigned i = 0; i != m; i++ ) {
			double t = iter*0.86 + i*(i + 0.1);
			for (unsigned j = 1; j != m; j++ ) {
				a[i][j] = a[i][j-1] + 3.1415*t;
				t += a[i][j];
			}
		}
	}
	for (unsigned iter = 0; iter < bk; iter++) {
		for (unsigned i = 0; i != m; i++ ) {
			double t = iter*0.86 + i*(i + 0.1);
			for (unsigned j = 1; j != m; j++ ) {
				a[i][j] = a[i][j-1] + 3.1415*t;
			}
		}
	}
	}
	return 0;
}

//+CA
//+TL 33 33  i | > | 1 | |
//+TL 34 34  i | > | 1 | |

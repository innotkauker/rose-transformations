#include <stdio.h>

int f(int x)
{
  int r = x;
  for (int i = 0; i < x; i++) 
    r *= r;
  return r;
}

void g(int x)
{
  int r = x;
  for (int i = 0; i < x; i++) 
    r *= r;
  printf("%i\n",r);
}

int h(int x,int y)
{
  int r = x + y;
  for (int i = 0; i < x - y; i++) 
    r *= r;
  printf("%i\n",r);
  return r;
}

int main()
{
  
#pragma x inline
  if (f(3)) 
    printf("true 1\n");
   else 
    printf("false 1\n");
  
#pragma x inline
  if (f(4) + f(5)) 
    printf("true 2\n");
   else if (h(6,2)) 
    printf("not true 2\n");
  
#pragma x inline
  _Bool rose__temp = 1;
  int rose_temp__4;
  int rose_temp__8;
  int _r4 = 4;
  for (int _i5 = 0; _i5 < 4; _i5++) 
    _r4 *= _r4;
  rose_temp__8 = _r4;
  int _r1 = rose_temp__8;
  for (int _i2 = 0; _i2 < rose_temp__8; _i2++) 
    _r1 *= _r1;
  rose_temp__4 = _r1;
  int rose_temp__13;
  int _r8 = 5 + 2;
  for (int _i9 = 0; _i9 < 5 - 2; _i9++) 
    _r8 *= _r8;
  printf("%i\n",_r8);
  rose_temp__13 = _r8;
  rose__temp = ((int )(rose_temp__4 + rose_temp__13));
  if (rose__temp) 
    printf("true 3\n");
}

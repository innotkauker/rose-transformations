#include <stdio.h>

int f(int* x)
{
	return *x + 1;
}

void g(int* x)
{
	*x = f(x);
}

int id(int x)
{
	return x;
}

int main()
{
	int a = 5;
	int r = 6;
	double b = 0.42;
	double c = 5e2;
	int results[6];
	#pragma x inline(id, f, g)
	for (int i = id(0); i < id(6); i++) {
		results[id(i)] = f(&i);
		g(&(results[id(i)]));
	}
	for (int i = 0; i < 6; i++)
		printf("%i ", results[i]);
}


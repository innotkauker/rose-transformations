#pragma once

#include "sage3basic.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <tuple>
#include <exception>
#include "RDAnalysis.h"
#include "LVAnalysis.h"

namespace TfUtils {

using CNS = std::set<const SgInitializedName*>;
using NS = std::set<SgInitializedName*>;
using CNV = std::vector<const SgInitializedName*>;
using NV = std::vector<SgInitializedName*>;
using CEV = std::vector<const SgExpression*>;
using EV = std::vector<SgExpression*>;
using CES = std::set<const SgExpression*>;
using ES = std::set<SgExpression*>;
using CSV = std::vector<const SgStatement*>;
using SV = std::vector<SgStatement*>;
using CNSV = std::vector<CNS>;
using STRS = std::set<std::string>;
using STRV = std::vector<std::string>;
using SWPT = std::tuple<std::size_t, std::size_t, bool>;
using INLT = std::tuple<std::string, bool, std::size_t>;

using DEP = std::pair<const SgInitializedName*, CNS>;
using depv = std::vector<DEP>;
using lref = std::pair<SgExpression*, SgStatement*>;
using reaching_defs = std::map<SgExpression*, lref>;
using rd_query = std::vector<std::pair<bool, SgExpression*>>;

using unique_node_ptr = std::unique_ptr<SgNode, decltype(&SageInterface::deepDelete)>;
using unique_expr_ptr = std::unique_ptr<SgExpression, decltype(&SageInterface::deepDelete)>;
using unique_stmt_ptr = std::unique_ptr<SgStatement, decltype(&SageInterface::deepDelete)>;
using unique_for_stmt_ptr = std::unique_ptr<SgForStatement, decltype(&SageInterface::deepDelete)>;
using shared_node_ptr = std::shared_ptr<SgNode>;

const std::size_t IDEPTH = 10;

/// Logger and logging functions
enum log_level {
	LOG_OFF = -2,
	LOG_DEF = -1,
	LOG_DBG,
	LOG_INFO,
	LOG_WARN,
	LOG_ERR,
};

class Logger {
	std::ostream *o;
	log_level default_level;
	log_level current_level;
	log_level min_level;
	unsigned errors;
public:
	Logger(log_level min_level = LOG_DBG, std::ostream *o = &std::clog, log_level default_level = LOG_DBG);
	void set_min_level(log_level lvl) { min_level = lvl; }
	unsigned get_errors() { return errors; }
	template<class T> Logger& operator<<(const T& msg)
	{
		if (current_level == LOG_DEF)
			(*this) << default_level;
		if (current_level >= min_level)
			(*o) << msg;
		return *this;
	}
	Logger& operator<< (log_level lvl);
	Logger& operator<< (std::ostream& (*pf)(std::ostream&));
};

extern Logger l;

class Exception;

std::ostream &operator<<(std::ostream &o, const SgInitializedName *x);
std::ostream &operator<<(std::ostream &o, const SgNode *x);
std::ostream &operator<<(std::ostream &o, const NS *in);
std::ostream &operator<<(std::ostream &o, const CNS *in);
std::ostream &operator<<(std::ostream &o, const NV *in);
std::ostream &operator<<(std::ostream &o, const CNV *in);
std::ostream &operator<<(std::ostream &o, const std::map<const SgPntrArrRefExp*, const SgExpression*> *in);
std::ostream &operator<<(std::ostream &o, const std::map<const SgInitializedName*, const SgExpression*> *in);
std::ostream &operator<<(std::ostream &o, const std::set<std::string>& in);
std::ostream &operator<<(std::ostream &o, const std::map<string, std::size_t>& in);
std::ostream &operator<<(std::ostream &o, const depv *in);
std::ostream &operator<<(std::ostream &o, const Exception& e);

//template<typename ContainerT>
//std::ostream &operator<<(std::ostream &o, const ContainerT& in)
//{
//        o << "{ ";
//        for (auto i : in) {
//                o << i << " ";
//        }
//        return o << "}";
//}

extern const std::string BORDER_ATTRIBUTE;
extern const std::string ID_ATTRIBUTE;
extern const std::string RD_ATTRIBUTE;
extern const std::string DIRV_ATTRIBUTE;

/// Our exception class that supports supplying a node that failed
///
class Exception: public std::exception {
	const std::string desc;
	const SgNode *_node0;
	const SgNode *_node1;
public:
	Exception(const std::string &err, const SgNode *node0 = nullptr, const SgNode *node1 = nullptr):
				desc(err), _node0(node0), _node1(node1) {}
	Exception(const char *err, const SgNode *node0 = nullptr, const SgNode *node1 = nullptr):
				desc(err), _node0(node0), _node1(node1) {}
	const std::string get_desc() const { return desc; }
	const SgNode* get_node0() const { return _node0; }
	const SgNode* get_node1() const { return _node1; }
	const char *what() const noexcept { return desc.c_str(); }
};

/// A class that is used for searching
/// for variable or array references in containers.
///
/// \todo Replace the sets with sorted vectors where appropriate,
/// make this class use binary search.
class RefFinder {
	// if searching for array reference
	const std::vector<const SgExpression*> *subexps = nullptr;
	// if searching for some variables
	const CNS *vars = nullptr;
	// if searching for some variable names
	const STRS *names = nullptr;
	bool negative;
public:
	RefFinder(const std::vector<const SgExpression*> *subexps, bool neg = false): subexps(subexps), negative(neg) {}
	RefFinder(const CNS *vars, bool neg = false) {this->vars = vars; this->negative = neg;}
	RefFinder(const STRS *names, bool neg = false) {this->names = names; this->negative = neg;}
	bool operator() (const SgInitializedName *p) const;
	bool operator() (const std::pair<const SgInitializedName* const, const SgExpression*> &p) const;

	template<typename MapKeyT, typename MapElemT>
	bool operator() (const std::pair<MapKeyT, MapElemT> &p) const;
};

/// Container for parameters found in pragmas.
///
typedef struct TransformParams {
	/// Variables for propagation.
	STRS propagate_vars;
	/// When propagating these variables,
	/// consider only local definitions.
	STRS propagate_locally;
	/// These variables are dead after the block.
	/// \todo currently unused.
	STRS local_variables;
	/// Bringout these variables.
	STRS bringout_vars;
	/// Split the loop using these variables.
	STRS split_vars;
	/// Permutate loops in a nest according to the order
	/// of their indices supplied in this vector.
	STRV permutation_indices;
	/// Map these.
	std::map<std::string, std::string> mapping_vars;
	/// Shift these indices.
	std::multimap<std::string, int> index_shift_vars;
	/// Unroll loops with these indices.
	std::multimap<std::string, int> unroll_vars;
	/// Unroll the entire loop.
	bool unroll{false};
	/// Remove the block.
	bool remove{true};
	/// Bringout all invariants.
	bool bringout_everything{false};
	/// Distribute the loop.
	bool distribute{false};
	/// Partition the loop body using the
	/// statements with 'border' attribute.
	bool partition{false};
	/// Attach a 'border' attribute to the next statement.
	bool border{false};
	/// Attach an 'id' attribute to the next statement.
	int id{-1};
	/// Swap loops with corresponding ids.
	std::vector<SWPT> swap_indices;
	/// Merge loops with corresponding ids.
	std::vector<std::vector<std::size_t>> merge_indices;
	/// Inline these functions
	STRV functions_to_inline;
	/// Inline everything
	bool inline_everything{false};
	/// Inlining depth
	std::size_t idepth{IDEPTH};
	/// Paralellize.
	bool paralellize{false};

	TransformParams();
} TransformParams;

enum class Direction {
	straight, // <
	none, // =
	backward, // >
	any, // *
	unspecified, // happens only on distinct iterations
};

/// Stores user-supplied dependency information.
///
class DirvAttribute: public AstAttribute {
public:
	typedef std::vector<Direction> DirV;
private:
	DirV directions;
public:
	DirvAttribute(DirV d): directions(d) {}
	const DirV& get_directions() { return directions; }
	virtual std::string attribute_class_name() const override
	{
		return std::string("DirvAttribute");
	}
	// TODO why would this be needed?
	// virtual AstAttribute* constructor() const override
	// {
		// return new IdAttribute(0);
	// }
	virtual AstAttribute::OwnershipPolicy
	getOwnershipPolicy() const override {
		return CONTAINER_OWNERSHIP;
	}


	virtual AstAttribute* copy() const override
	{
		return new DirvAttribute(directions);
	}
};

typedef struct AnalysisParams {
	bool reduction{false};
	bool dependency{false};
	DirvAttribute::DirV dependency_vector;
} AnalysisParams;

/// An attribute to attach to loops with assigned ids.
///
class IdAttribute: public AstAttribute {
	int id;
public:
	IdAttribute(int i): id(i) {}
	int get_id() { return id; }
	virtual std::string attribute_class_name() const override
	{
		return std::string("IdAttribute");
	}
	// TODO why would this be needed?
	// virtual AstAttribute* constructor() const override
	// {
		// return new IdAttribute(0);
	// }

	virtual AstAttribute* copy() const override
	{
		return new IdAttribute(id);
	}
};

class AttributeFinder {
	int target_id;
public:
	AttributeFinder(int id) { target_id = id; }
	bool operator() (const SgStatement *s);
};

/// Helping functions follow
bool is_a_loop(const SgStatement* stmt);
bool is_a_var_reference(const SgNode* node);
bool is_modified(const SgNode* node, bool* is_read = nullptr);
bool is_output(const SgNode* node);
SgBasicBlock *get_nested_loops_main_block(SgStatement* loops, int depth = -1);
std::vector<SgForStatement*> get_all_nested_loops(SgForStatement* loops);
std::vector<SgForStatement*> get_all_surrounding_loops(SgStatement* stmt);
bool loops_have_equal_heads(SgForStatement* l0, SgForStatement* l1);
int get_merge_head_depth(const std::vector<SgForStatement*> &nest0, const std::vector<SgForStatement*> &nest1);
std::vector<SgForStatement*> query_loop_nests(SgNode* root);

bool expr_contains_any_of_vars(const SgExpression *expr, const SgInitializedName* var);
bool expr_contains_any_of_vars(const SgExpression *expr, const CNS &vars);
void get_secondary_referenced_vars(const SgExpression *expr, CNS &where);
void get_secondary_referenced_vars(const SgStatement* stmt, CNS &where);

bool node_references_var(const SgNode *node, const std::string &var);
bool is_child_of(const SgStatement *definition, const SgStatement *block);
bool is_modifier(const SgAssignOp *assignment);
bool equal_as_strings(SgNode* a, SgNode* b);
std::string strip(std::string&& s);

bool is_2d_variable_declaration(const SgStatement *s);
void output_statement_alteration(const std::string &orig, const SgStatement *changed);

SV split_block_into_statements(SgStatement *block);
bool express_var_in_rhs(SgExpression*& lhs, SgExpression*& rhs, const string &target_var);
bool express_var_in_rhs(SgExpression*& lhs, SgExpression*& rhs, SgInitializedName* target_var);
void remove_statement(SgStatement *stmt);
SgInitializedName* get_var_initialized_name(const SgNode *e);
extern SgInitializedName* (&initialized_name)(const SgNode *e);
SgInitializedName* same_var_referenced(SgNode* a, SgNode* b);
std::string get_var_name(const SgNode *n);
std::string get_var_name_from_node(const SgExpression *e);
long long getSignedIntegerConstantValue(SgValueExp* expr);
SgExpression *get_unary_modifier_expression(SgExpression *um);
const SgExpression* skip_casting(const SgExpression* exp);
SgExpression* skip_casting(SgExpression* exp);
SgExpression* remove_casting(SgExpression* e, bool only_generated = true);
void propagate_constants(SgExpression *e);
const SgInitializedName* convertRefToInitializedName(const SgNode *current);
const SgInitializedName* get_declared_var(const SgVariableDeclaration* d);
bool isArrayReference(const SgExpression* ref,
			const SgExpression **arrayName = nullptr,
			CEV **subscripts = nullptr);
EV get_subscripts(SgPntrArrRefExp* a);
bool isAssignmentStatement(const SgNode *s, const SgExpression **lhs = nullptr,
				const SgExpression **rhs = nullptr,
				bool *readlhs = nullptr);
bool isSomeAssignment(const SgNode* s, const SgInitializedName** lhs = nullptr,
					const SgExpression** rhs = nullptr);
bool isInitializedVariableDeclaration(const SgNode* s,
					const SgInitializedName** lhs = nullptr,
					const SgExpression** rhs = nullptr);
bool modifiedForLoopNormalization(SgForStatement* loop);
void loop_interchange(SgForStatement* loop, size_t depth,
			std::vector<size_t> &changed_order);
void get_live_variables(SPRAY::LVAnalysis *lva, SgStatement *stmt,
			set<string> *live_ins, set<string> *live_outs);
CNS convert_names_to_vars(SgScopeStatement* block, const STRS& vars);
bool isPureAssignment(SgNode* s, SgInitializedName** lhs = nullptr,
			SgExpression** rhs = nullptr);
bool isPureAssignment(const SgNode* s, const SgInitializedName** lhs = nullptr,
			const SgExpression** rhs = nullptr);
bool isPureAssignment(SgNode* s, SgExpression** lhs,
			SgExpression** rhs = nullptr);

/// Wrappers for SPRAY analyses.
std::unique_ptr<SPRAY::LVAnalysis> perform_liveness_analysis(SgFunctionDefinition *root, bool log = false);

/// Has reaching definitions for the whole project.
/// Can query them or check propagation correctness.
class DefinitionProvider {
	SgFunctionDefinition* f;
	bool log{false};
	unique_ptr<SPRAY::RDAnalysis> rda;

	bool select_reaching_definition(SgExpression *ref, SV&& definitions,
				lref &result, SgStatement* root = nullptr);
public:
	DefinitionProvider(SgFunctionDefinition* f, bool log = false);
	void regenerate();
	void invalidate();
	bool valid() { return rda != nullptr; }
	void set_log(bool l) { log = l; }
	void get_definitions(SgStatement* stmt, const rd_query& refs,
			reaching_defs& result, SgStatement* root = nullptr);
	void get_definitions(SgStatement* stmt, const ES& refs,
					reaching_defs& result);
	bool propagation_was_correct(const lref &reaching_definition,
					const lref &propagated_definition);
};

/// A wrapper for body getters for 3 types of C loops.
SgStatement* get_loop_body(const SgForStatement *loop);
SgStatement* get_loop_body(const SgWhileStmt *loop);
SgStatement* get_loop_body(const SgDoWhileStmt *loop);

/// Gets an expression with loop's indices for 3 types of loops.
/// It is condition for do- and while loops and increment for for-loops.
SgNode* get_exp_with_loop_indices(const SgForStatement *loop);
SgNode* get_exp_with_loop_indices(const SgWhileStmt *loop);
SgNode* get_exp_with_loop_indices(const SgDoWhileStmt *loop);

using CopyImpl = std::function<SgNode*(SgNode*)>;
SgBasicBlock* inline_function(SgFunctionCallExp* funcall, bool allowRecursion = false, CopyImpl copyImpl = CopyImpl());

template<typename ContainerT, typename PredicateT>
void erase_if(ContainerT& items, const PredicateT& predicate);

template<typename... BoolT>
int how_many_of(BoolT... args);

template <typename NodeType>
std::vector<const NodeType*> querySubTree(const SgNode *top,
		VariantT variant = (VariantT)NodeType::static_variant);

template <typename NodeType, typename ContainerT>
void query_subtree(SgNode *top, ContainerT &result,
		VariantT variant = (VariantT)NodeType::static_variant);

template <typename NodeType, typename ContainerT>
void query_subtree(const SgNode *top, ContainerT &result,
			VariantT variant = (VariantT)NodeType::static_variant);

// TODO refactor these functions into something usable?

template<typename NodeT>
bool collectReadWriteRefs(const NodeT *stmt, std::vector<const SgNode*> &readRefs,
				std::vector<const SgNode*> &writeRefs);

template<typename NodeT, typename ContainerT>
void compute_read_write_vars(const NodeT* node, ContainerT &read_vars, ContainerT &write_vars);

template<typename NodeT, typename ContainerT>
void get_rw_vars(const NodeT* node, ContainerT &read_vars, ContainerT &write_vars);

template<typename ContainerT>
std::vector<CNS> get_vars_modified_in_statements(const ContainerT& statements);

template<typename NodeT, typename ContainerT>
void get_referenced_vars(const NodeT* node, ContainerT &result);

template<typename NodeT>
NS get_referenced_vars(NodeT* node);

template<typename NodeT, typename ContainerT>
void get_readonly_vars(const NodeT* node, ContainerT &result);

template<typename NodeT, typename ContainerT>
void get_modified_variables(NodeT *node, ContainerT &result, bool only_update = false);

template<typename LoopT, typename ContainerT>
void get_nested_loops_indices(const LoopT *loops, ContainerT &loop_indices);

template<typename LoopT>
SgStatement* get_nested_loops_body(const LoopT *loops, int depth = -1);

template<typename NodeT, typename ContainerT>
void get_all_references(NodeT *node, ContainerT &container);

void get_top_references(SgNode *node, EV &r, EV &w);

template<typename NodeT, typename SourceT, typename ContainerT>
void get_enclosing_nodes(SourceT *node, ContainerT &container, bool including_self = true);

template<typename NodeT,
	std::enable_if_t<
			std::is_same<std::remove_const_t<NodeT>,
					SgStatement*>::value
			>* = nullptr
	>
auto stmt_to_expr(NodeT s);

template <typename NodeType>
NodeType* getEnclosingNode(SgNode* node, SgNode* stopAt = nullptr,
				const bool includingSelf = false);

}

#include "util.tpp"


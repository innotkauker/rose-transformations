#pragma once
#include "sage3basic.h"
#include "unparseFormatHelp.h"

class UnparseFormatCustom: public UnparseFormatHelp {
	int indentation{8};
	int max_line_lenght{140};

	virtual int tabIndent()
	{
		return indentation;
	}
	virtual int maxLineLength()
	{
		return max_line_lenght;
	}

public:
	UnparseFormatCustom(int indentation = 8): indentation(indentation) {}
};

#pragma once

#include "sage3basic.h"
#include "util.h"

/// The class that performs transformations.
///
class CodeTransformer {
	TfUtils::TransformParams p;

	/// The transformed block
	SgStatement *block;

	/// If a loop is being handled - its body and indices,
	/// otherwise *body_block* == *block*.
	SgStatement *body_block;
	TfUtils::CNS loop_indices;

	void transformation_compatibility_check();
	void construct_for_block(SgBasicBlock *block);
	void construct_for_loop(SgForStatement *for_loop);
	void construct_for_scope(SgScopeStatement *scope);
	void construct_for_stmt(SgStatement *stmt);

	void perform_bringout();
	void remove_temporary_block(SgBasicBlock *block);
public:
	CodeTransformer(SgStatement *the_block,
		TfUtils::TransformParams&& params);
	void handle_block();
};


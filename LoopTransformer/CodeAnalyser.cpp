#include "CodeAnalyser.h"
#include "sage3basic.h"
#include "util.h"
#include "ReductionAnalysis.h"
#include "DependencyAnalysis.h"

using namespace SageInterface;
using namespace TfUtils;

CodeAnalyser::CodeAnalyser(SgStatement* block, TfUtils::AnalysisParams&& params)
: p(params), block(block)
{
}

CodeAnalyser::~CodeAnalyser() = default;

void CodeAnalyser::handle_block()
{
	if (!p.dependency_vector.empty()) {
		using TfUtils::DirvAttribute;
		auto nest = isSgForStatement(block);
		assert(nest);
		l << "Using pre-defined dependency vector for loop " << nest << std::endl;
		nest->setAttribute(DIRV_ATTRIBUTE, new DirvAttribute(p.dependency_vector));
	}

	using namespace CodeAnalyses;
	for (auto loop : query_loop_nests(block)) {
		if (p.reduction) {
			ReductionAnalysis a(loop);
			a.run(true);
		}
		if (p.dependency) {
			DependencyAnalysis a(loop);
			a.run();
			const auto& result = a.get_result();
			for (const auto& d : result) {
				d.dump();
			}
			//a.annotate_source();
		}
	}
}

#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y0 = 3, y1 = 3, y2 = 5, yt = 6;
	double t0 = 0, t1 = 1, t2 = 2;
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		t0 -= c[i];
	for (int i = 0; i < m; i++)
		t1 *= c[i];
	for (int i = 0; i < m; i++)
		t2 /= c[i];
	for (int i = 0; i < m; i++)
		y0 &= ct[i];
	for (int i = 0; i < m; i++)
		y1 |= ct[i];
	for (int i = 0; i < m; i++)
		y2 ^= ct[i];
	}
	return 0;
}
//+T pragma PRG REDUCTION(DIFF(t0))
//+T pragma PRG REDUCTION(PRODUCT(t1))
//+T pragma PRG REDUCTION(RATIO(t2))
//+T pragma PRG REDUCTION(BINAND(y0))
//+T pragma PRG REDUCTION(BINOR(y1))
//+T pragma PRG REDUCTION(XOR(y2))

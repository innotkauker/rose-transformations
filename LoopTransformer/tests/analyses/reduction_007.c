#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 3, yt = 6, yta = 1, ytb = 1, ytc = 1, ytd = 1, yte = 1;
	double td = 0, tp = 1, tt = 2;
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		td = td - c[i];
	for (int i = 0; i < m; i++)
		tp = tp * c[i];
	for (int i = 0; i < m; i++)
		tt = tt / c[i];
	for (int i = 0; i < m; i++)
		yta = yta & ct[i];
	for (int i = 0; i < m; i++)
		ytb = ytb | ct[i];
	for (int i = 0; i < m; i++)
		ytc = ytc ^ ct[i];
	for (int i = 0; i < m; i++)
		ytd = ytd || ct[i];
	for (int i = 0; i < m; i++)
		yte = yte && ct[i];
	}
	return 0;
}
//+T pragma PRG REDUCTION(DIFF(td))
//+T pragma PRG REDUCTION(PRODUCT(tp))
//+T pragma PRG REDUCTION(RATIO(tt))
//+T pragma PRG REDUCTION(BINAND(yta))
//+T pragma PRG REDUCTION(BINOR(ytb))
//+T pragma PRG REDUCTION(XOR(ytc))
//+T pragma PRG REDUCTION(OR(ytd))
//+T pragma PRG REDUCTION(AND(yte))

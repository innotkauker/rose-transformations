#include <stdio.h>
#include <assert.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	const unsigned k = 3;
	int a[m][n];
	double bc[k][k];
	double bs[k][k];
	int j, i;
	double t = 0, t1 = 3, t2 = 5;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;
	for (unsigned i = 0; i < k; i++)
		for (unsigned j = 0; j < k; j++) {
			bs[i][j] = i*i*3 + i;
			bc[i][j] = j*k*6 - 5*j + i + 8;
		}

	#pragma x merge(0, 2) nontemp
	{
	#pragma x id(0)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			a[i][j] = j*3;
		}

	#pragma x id(2)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = i*j*2;
			a[i][j] += tmp;
		}
	}

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%d \n", a[i][j]);

	return 0;
}

#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5;
	#pragma x bringout
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			int x = j, y = i;
			t1 = x;
			t = y + t1 + 1;
			#pragma x unroll
			for (int k = 0; k < 4; k++) {
				int tt = t + k;
				a[i][j] -= tt;
			}
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

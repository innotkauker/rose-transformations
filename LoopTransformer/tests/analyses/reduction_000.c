#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, total = 0, t = 0;
	double t0 = 0;
	double t1 = 0;
	double t2 = 0;
	double t3 = 0;
	double t4 = 0;
	double t5 = 0;
	double t6 = 0;
	#pragma a reduction
	for (int i = 0; i < m; i++) {
		t2 += c[i];
	}
	printf("%f", total);
	t0 = 0;
	#pragma a reduction
	for (int i = 0; i < 4; i++) {
		t0 += a[i][0];
	}
	#pragma a reduction
	for (int i = 0; i < 4; i++) {
		a[i][0] = 2*t;
		b[i][0] = 3*t;
		t1 += a[i][0] + b[i][0];
	}
	printf("%f", t1);
	#pragma a reduction
	for (int i = 0; i < 4; i++) {
		t3 += i*i;
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}
//+T pragma PRG REDUCTION(SUM(t0))
//+T pragma PRG REDUCTION(SUM(t2))
//+T pragma PRG REDUCTION(SUM(t3))

#include <stdio.h>

int main()
{
	const unsigned n = 2;
	const unsigned m = 2;
	double a[m][n];
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j/1.2 - 5*j + i/1.4;

	#pragma x unroll
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			a[i][j] += i*j*4;
		}

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%f \n", a[i][j]);

}

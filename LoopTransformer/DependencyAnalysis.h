#pragma once
#include <memory>
#include <set>
#include <vector>
#include <deque>
#include "sage3basic.h"
#include "util.h"

namespace CodeAnalyses {

using TfUtils::Direction;

enum class AccessType {
	r, w, rw
};

struct DataAccess {
	SgExpression* e;
	AccessType t;

	DataAccess(SgExpression* e);
};

struct LoopInfo {
	SgForStatement* loop{nullptr};
	bool canonical{false};
	SgInitializedName* name{nullptr};
	// If loop is not canonical, it can have
	// more than one index. Store them here...
	TfUtils::NS all_indices;
	SgExpression* lb{nullptr};
	SgExpression* ub{nullptr};
	int step{0};

	LoopInfo(SgForStatement* loop);
};

/// Represents the concept of dependency.
/// Source here is which access happens first, sink is which is second (I think).
/// Source and sink contain either references to the same variable
/// or access to elements of one array. In both cases information
/// on whether read or write happens is stored.
class Dependency {
public:
	enum class OperationType {
		unknown,
		flow,
		anti,
		output,
		input,
	};
	enum class OrderType {
		forward,
		self,
		backward,
	};
	class LoopInfo;

private:
	SgStatement* source{nullptr};
	SgStatement* sink{nullptr};
	SgInitializedName* variable{nullptr};

	OperationType operation_type{OperationType::unknown};

	bool present{true};

	std::unique_ptr<LoopInfo> loop_info;

public:
	Dependency(): present(false) {}
	Dependency(DataAccess src, DataAccess snk);
	Dependency(DataAccess src, DataAccess snk, SgInitializedName* var);
	Dependency(SgExpression* src, SgExpression* snk);
	Dependency(DataAccess src, DataAccess snk, const TfUtils::NS& indices);
	Dependency(SgExpression* src, SgExpression* snk, const TfUtils::NS& indices);
	Dependency(SgForStatement* loop, const std::deque<SgInitializedName*>& indices);
	Dependency(Dependency&& d) = default;
	Dependency& operator=(Dependency&& d) = default;
	Dependency(const Dependency& d);
	Dependency& operator=(const Dependency& d);
	~Dependency() = default;
	void merge(Dependency&& b);
	Dependency&& splice(Dependency&& b) &&;
	auto get_source() const { return source; }
	auto get_sink() const { return sink; }
	auto get_operation_type() const { return operation_type; }
	auto get_var() const { return variable; }
	bool is_loop_carried() const { return loop_info != nullptr; }
	bool hopeless() const;
	auto& loop() { return *loop_info; }
	const auto& loop() const { return *loop_info; }
	bool exists() const { return present; }
	void make_independent() { present = false; }
	void reset();
	void finalize();
	void dump() const;
};

/// If the dependency is loop-carried, then two references to one array happen.
/// In this case we can get more precise information, like which concrete
/// iterations bring the dependency, a direction vector or a
/// a distance vector.
class Dependency::LoopInfo {
	TfUtils::NV indices;
	std::vector<Direction> dirv;
	std::vector<int> distv;
	std::vector<std::set<int>> iterv;
	OrderType order_type; // TODO what is this?

	friend ostream& operator<<(ostream& o, Direction s);
public:
	LoopInfo(const TfUtils::NS& i);
	LoopInfo(const std::deque<SgInitializedName*>& i);
	// LoopInfo(SgInitializedName* i) : indices(1, i), dirv(1, Direction::any)
	// {}
	const auto& get_directions() const { return dirv; }
	const auto& get_distances() const { return distv; }
	const auto& get_iterations() const { return iterv; }
	const auto& get_indices() const { return indices; }
	Direction get_direction(SgInitializedName *name) const;
	void merge(LoopInfo&& other);
	void splice(LoopInfo* other);
	void normalize();
	// Directions can perhaps be turned into differences,
	// but otherwise these are incompatible --
	// if something is added at i, nothing else should be set for i before that.
	void set_direction(unsigned i, Direction d);
	void set_distance(unsigned i, int d);
	void add_iteration(unsigned i, int it);
	OrderType get_order_type() const { return order_type; }
};

class DependencyAnalysis {
	class Impl;
	std::unique_ptr<Impl> impl;
public:
	DependencyAnalysis(SgForStatement* loop);
	~DependencyAnalysis();
	void run();
	void annotate_source();
	void annotate_ast();
	const std::vector<Dependency>& get_result() const;
	std::vector<Dependency> take_result();
};

TfUtils::NV get_private_variables(SgForStatement* loop);

} // namespace CodeAnalyses

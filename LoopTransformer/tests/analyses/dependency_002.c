#include <stdio.h>

// Single-sized subscripts, Weak-zero SIV
int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,6,7,8};
	int cc[] = {1,2,3,4,5,6,6,7,8};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, total = 0, t = 0;
	const int N = 1;
	const int M = 2;
	#pragma a dependency
	{
	for (int i = 0; i < m; i++)
		c[5] += c[i];
	for (int i = 0; i < m; i++)
		c[i] = c[5] + 5;
	for (int i = 0; i < m; i++)
		c[1] = c[i + m] + 5;
	for (int i = 0; i < m; i++)
		c[0*i + 800] = c[i - 800] + 5;
	for (int i = 0; i < m; i++)
		c[0] = c[i-1] - a[i][0];
	for (int i = 0; i < m; i++)
		c[2*i + 1] = c[m-1];
	for (int i = 1; i < m; i++) {
		c[0] = c[i];
		c[0] *= 2;
		c[i-1] = c[0];
	}
	for (int i = 1; i < m; i++) {
		c[i] = c[0];
		c[i] *= 2;
		c[0] = c[i];
	}
	}
	return 0;
}
//+CA
//+TL 40 40 i | \\* | | |
//+FL 40 40 i | | | 5 |
//+FL 42 42 i *
//+FL 44 44 i *
//+FL 46 46 i *
//+TL 48 48 i | | | 1 |
//+TL 50 50 i | | | 1 |
//+FL 52 52 i | | | 0 |
//+TL 52 53 i | \\* | | |
//+TL 52 54 i | \\* | | |
//+TL 52 54 i | < | 1 | |
//+TL 53 53 i | \\* | | |
//+TL 53 54 i | | | 1 |
//+TL 53 54 i | \\* | | |
//+TL 54 54 i | | | 1 |
//+TL 57 59 i | \\* | | |
//

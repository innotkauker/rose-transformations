#include <stdio.h>

// Check some more inter-procedural stuff.

#define n 4
#define m 4

void full_dep(double x[n])
{
	for (unsigned j = 1; j != m; j++ )
		x[j] = x[j-1] + 3.1415;
}

int main()
{
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,6,7,8};
	int cc[] = {1,2,3,4,5,6,6,7,8};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	#pragma a dependency
	{
	for (unsigned iter = 0; iter < bk; iter++) {
		for (unsigned i = 0; i != m; i++ ) {
			full_dep(a[i]);
		}
	}
	}
	return 0;
}

//+CA
//+TL 11 11  i | > | 1 | |

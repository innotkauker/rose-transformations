#include "DynamicAnalysis.h"
#include <cstdio>
#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <array>
#include <ctime>
#include <regex>
#include <boost/range.hpp>
#include <boost/algorithm/string.hpp>

using namespace TfUtils;

const std::string PROFILE_ATTRIBUTE = "profile";

Profiler::Instrumentation Profiler::create_inst(SgStatement *stmt, unsigned index)
{
	Instrumentation inst;
	inst.stmt = stmt;
	auto scope = SageInterface::getEnclosingScope(stmt, true);
	using namespace SageBuilder;
	// auto clock_type = buildFunctionType(buildUnsignedIntType(), buildFunctionParameterList());
	auto start_clock_call = buildFunctionCallExp(SgName("clock"), buildUnsignedIntType(), nullptr, scope);
	auto end_clock_call = buildFunctionCallExp(SgName("clock"), buildUnsignedIntType(), nullptr, scope);
	std::stringstream timer_name;
	timer_name << "_timer" << index;
	auto init = buildAssignInitializer(start_clock_call, buildUnsignedIntType());
	inst.prestart = buildVariableDeclaration(timer_name.str(), buildUnsignedIntType(), init);
	auto end = buildAssignOp(buildVarRefExp(timer_name.str()),
				buildSubtractOp(end_clock_call, buildVarRefExp(timer_name.str())));

	EV printf_params = { buildStringVal(string("\\nLoopTransformer profile: ")
						+ string(timer_name.str()) + string(": %f seconds\\n")),
				buildDivideOp(buildCastExp(buildVarRefExp(timer_name.str()), buildDoubleType()),
						buildIntVal(CLOCKS_PER_SEC)) };
	auto result_print = buildFunctionCallExp(SgName("printf"), buildVoidType(),
						buildExprListExp(printf_params), scope);
	inst.postend = buildBasicBlock(buildExprStatement(end), buildExprStatement(result_print));
	return inst;
}

// Execute cmd and return the output
std::string exec(std::string cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
	if (!pipe)
		throw Exception("popen() failed while trying to profile the program.");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != NULL)
			result += buffer.data();
	}
	return result;
}

Profiler::Profiler(SgProject* project)
{
	std::string time_include = "#include <time.h>";
	std::string io_include = "#include <stdio.h>";
	auto include_type = PreprocessingInfo::CpreprocessorIncludeDeclaration;
	for (auto f : project->get_files()) {
		auto file = isSgSourceFile(f);
		if (!file) {
			l << LOG_WARN << "Profiler: found non-source file" << file->getFileName() << std::endl;
		}
		l << "Profiler: found file " << file->getFileName() << std::endl;
		SageInterface::attachComment(file, time_include, include_type);
		SageInterface::attachComment(file, io_include, include_type);
		for (auto function : SageInterface::querySubTree<SgFunctionDefinition>(file))
			boost::copy(TfUtils::query_loop_nests(function), std::back_inserter(targets));
	}
}

void Profiler::profile()
{
	using boost::adaptors::indexed;
	using namespace SageInterface;
	// Insert the instrumentation
	for (auto target : targets | indexed()) {
		auto inst = create_inst(target.value(), target.index());
		instruments.push_back(inst);
		insertStatementBefore(inst.stmt, inst.prestart);
		insertStatementAfter(inst.stmt, inst.postend);
	}
	// Build the instrumented binary
	backend(getProject());
	// Remove the instrumentation
	for (const auto &inst : instruments) {
		removeStatement(inst.prestart);
		removeStatement(inst.postend);
	}
	// TODO this part can be asynchronous
	// TODO can we change WD/a.out name?
	auto results = exec("./a.out 2>&1");
	// Grep the profiler results and sum them up in the instruments vector.
	std::regex profile_regex("^LoopTransformer profile: _timer([0-9]*): ([0-9.]*) seconds$");
	std::smatch match;
	TfUtils::STRV lines;
	boost::split(lines, results, boost::is_any_of("\n"));
	for (auto line : lines)
		if (std::regex_match(line, match, profile_regex)) {
			assert(match.size() == 3);
			auto target = std::stoi(match[1].str());
			auto result = std::stod(match[2].str());
			assert(target >= 0 && (unsigned)target < instruments.size());
			instruments[target].time += result;
		}
}

void Profiler::dump()
{
	for (const auto& inst : instruments) {
		l << inst.stmt << " takes " << inst.time << " seconds." << std::endl;
	}
}

void Profiler::annotate()
{
	for (const auto& inst : instruments) {
		inst.stmt->setAttribute(PROFILE_ATTRIBUTE, new ProfileAttribute(inst.time));
	}
}

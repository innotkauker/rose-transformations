#include <stdio.h>

int f(int *x)
{
        return  *x + 1;
}

void g(int *x)
{
         *x = f(x);
}

int id(int x)
{
        return x;
}

int main()
{
        int a = 5;
        int r = 6;
        double b = 0.42;
        double c = 5e2;
        int results[6];
        for (int i = id(0); i < id(6); i++) {
                int rose_temp__4;
                int _x_20 = i;
                rose_temp__4 = _x_20;
                int *_x_61 = &i;
                results[rose_temp__4] =  *_x_61 + 1;
                int rose_temp__14;
                int _x_123 = i;
                rose_temp__14 = _x_123;
                int *_x_92 = &results[rose_temp__14];
                int *_x_164 = _x_92;
                 *_x_92 =  *_x_164 + 1;
        }
        for (int i = 0; i < 6; i++) 
                printf("%i ",results[i]);
}

#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0;
	#pragma x propagate(t, t1, vol)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			vol = 0.5*i;
			t = 15*j;
			t1 = i;
			for (t1 = i; t1 > 0; t1 -= 2)
				t *= vol;
			if (t1 < 0) {
				t1 = 5;
				for ( ; t1 > 0; t1--)
					t /= vol;
			}
			a[i][j] /= vol;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}


#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0;
	double total = 0, total0 = 0, t0 = 0;
	#pragma a reduction
	for (int i = 0; i < m; i++) {
		total += c[i];
		vol0 += c[i];
	}
	printf("%f", total);
	total0 = 0;
	#pragma a reduction
	for (int i = 0; i < 4; i++) {
		total0 += a[i][0];
	}
	printf("%f", total0);
	t0 = 0;
	#pragma a reduction
	for (int i = 0; i < 4; i++) {
		a[i][0] = 2*t;
		b[i][0] = 3*t;
		t0 += a[i][0] + b[i][0];
	}
	printf("%f", t0);

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}
//+T pragma PRG REDUCTION(SUM(vol0), SUM(total))
//+T pragma PRG REDUCTION(SUM(total0))

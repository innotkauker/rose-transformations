#include <stdio.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	const unsigned k = 3;
	int a[m][n];
	double bc[k][k];
	double bs[k][k];
	double c[k];
	int j, i;
	double t = 0, t1 = 3, t2 = 5;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;
	for (unsigned i = 0; i < k; i++) {
		c[i] = i*k*6 - 3*i - 2;
		for (unsigned j = 0; j < k; j++) {
			bs[i][j] = i*i*3 + i;
			bc[i][j] = j*k*6 - 5*j + i + 8;
		}
	}

	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(i1, i2, t, c2, c, vol) map(bs:bc)
		for (unsigned j = 0; j < 4; j++) {
			int i1, i2 = 2;
			i1 = 1;
			bs[i1][i1] = a[0][1] - a[i][j];
			bs[i1][i2] = a[1][2] + a[j][i];
			bs[i2][i1] = a[3][0] - a[j][i];
			bs[i2][i2] = a[0][3] + a[i][j];

			bc[i1][i1] = bs[2][2];
			bc[i1][i2] = -bs[2][1];
			bc[i2][i1] = -bs[1][2];
			bc[i2][i2] = bs[1][1];
			vol = bs[1][1] * bs[2][2] - bs[1][2] * bs[2][1];
			a[i][j] = - vol + t;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}


#pragma once

#include "sage3basic.h"
#include <string>
#include <set>
#include <vector>
#include <map>
#include "util.h"
#include "LoopUnroller.h"
#include "LoopSplitter.h"

/// This class performs variable propagation and mapping.
///
/// Target can be a block or a single SgExprStatement.
class BlockTraveller {

public:
	/// Parameters for BlockTraveller.
	///
	/// * *Propagation* contains pairs (variable, local).
	///   If *local* is true then only definitions found
	///   in the handled block will be considered.
	/// * *Mapping* specifies what variables should be mapped.
	struct BTParams {
		std::set<std::string> propagate;
		std::set<std::string> local;
		std::map<std::string, std::string> mapping;
		bool remove_unused{true};
	};

private:

	// variables
	BTParams p;
	SgStatement *target;
	bool log;
	TfUtils::DefinitionProvider rd;

	std::map<const SgInitializedName*, TfUtils::lref> mapped_vars;
	std::map<const SgPntrArrRefExp*, TfUtils::lref> mapped_arrays;

	std::map<std::string, std::size_t> p_stats;

	// methods
	void dispatch_stmt(SgStatement *stmt);
	void handle_scope_statement(SgStatement *stmt);
	void handle_expr_statement(SgExprStatement *statement);
	void handle_variable_declaration(SgVariableDeclaration *stmt);

	void propagate_omitting_top_var(SgExpression *lhs,
					SgStatement *statement);
	void propagate_into_expr_statement(SgExprStatement *statement);
	void propagate_selected_variables(SgExpression *expr_to_extend,
					SgStatement* enclosing_statement);
	const SgExpression* get_initializer(const SgExpression* ref,
				const SgVariableDeclaration* initialization);
	bool expr_can_be_used_for_propagation(const SgInitializedName *var,
					const SgExpression *expr);
	TfUtils::reaching_defs get_definitions(SgStatement *stmt,
					TfUtils::ES&& refs);
	bool find_mapping_definition(const SgExpression *ref,
					TfUtils::lref &result);
	void remove_unused_assignments(bool remove_everything);
	bool removal_possible();

	bool try_to_use_for_mapping(SgExprStatement *statement);

public:
	BlockTraveller(SgStatement *target, BTParams&& params, bool log = true);
	void traverse_block();
};

/// This one inlines function calls.
void inline_calls(SgStatement *stmt, TfUtils::STRV&& names, std::size_t depth,
			TfUtils::CopyImpl copy_impl = TfUtils::CopyImpl());

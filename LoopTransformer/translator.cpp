#include "PragmaProcessor.h"

int main(int argc, char * argv[])
{
	PragmaProcessor t(argc, argv);
	if (!t.needs_processing())
		return 0;
	t.process();
	return t.backend();
}


#!/usr/bin/env bash

declare -A transformations
transformations[index]=x;
transformations[id]=i;
transformations[unroll]=u;
transformations[permut]=P;
transformations[merge]=M;
transformations[swap]=S;
transformations[map]=m;
transformations[propagate]=p;
transformations[split]=s;
transformations[bringout]=b;
transformations[nontemp]=n;
transformations[inline]=I;
transformations[distribute]=d;

declare -A analyses
analyses[reduction]=r;
analyses[independent]=i;

declare -nl orders="$1"
ORDER=$2;
echo -n "|";
if [ "_$ORDER" == "_file" ]; then
	FILE=$3;
	for p in "${!orders[@]}"; do
		if [ "x$(grep $p $FILE)" != "x" ]; then
			echo -n ${orders[$p]};
		else
			echo -n ' ';
		fi;
		echo -n "|";
	done;
elif [ "_$ORDER" == "_header" ]; then
	for p in "${!orders[@]}"; do
		echo -n $p;
		echo -n "|";
	done;
else
	for p in "${!orders[@]}"; do
		echo -n "-|";
	done;
fi;

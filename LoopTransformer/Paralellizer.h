#pragma once
#include "util.h"
#include "ReductionAnalysis.h"

class SgProject;

// Inserts omp pragmas based on dependency analysis results (more features wanted).
class Paralellizer {
	SgNode *root;
	void paralellize(SgForStatement* loop, const std::vector<CodeAnalyses::Reduction>& reductions);
public:
	Paralellizer(SgNode* root);
	~Paralellizer();
	void run();
};


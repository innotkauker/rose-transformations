#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t, t1, t2;
	double vol;
	#pragma x propagate(t, t1, vol)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			vol = 0.5*i;
			t = 15*j;
			t1 = i;
			if (t1-- > 0)
				t *= vol;
			if (t1 < 0)
				t /= vol;
			a[i][j] /= vol;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}


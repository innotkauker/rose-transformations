#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        int a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        int t;
        int t1;
        int t2;
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        double vol = 0.1;
                        t1 = i;
                        while(t1-- > 0){
                                if ((vol += 0.3) > 2) 
                                        vol = vol + 0.2;
                                 else 
                                        vol = vol + 0.4;
                                for (int ii = 0; ii < 3; ii++) {
                                        int tmp = (ii + i);
                                        vol = vol + 0.1 * ((double )tmp);
                                }
                        }
                        a[i][j] = (((double )15) / vol);
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}

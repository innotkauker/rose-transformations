#define BOOST_RESULT_OF_USE_DECLTYPE
#include "PragmaProcessor.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <type_traits>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <boost/range/adaptors.hpp>
#include "sage3basic.h"
#include "UnparseFormat.h"
#include "CodeTransformer.h"
#include "CodeAnalyser.h"
#include "util.h"
#include "MintConstantFolding.h"
#include "StatementTransformations.h"
#include "DynamicAnalysis.h"
#include "Paralellizer.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace AstFromString;
using namespace NodeQuery;
using namespace TfUtils;

namespace {
/// The keyword for transfromation pragmas,
/// as *x* in `#pragma x nontemp`.
const char* TF_KWD = "x";
/// The keyword for analysis pragmas,
/// like *y* in `#pragma y reduction`.
const char* AN_KWD = "a";

const char* BUILD_INT_FILENAME = ".make.filenames";
} // namespace

static void collect_arguments(std::vector<std::string>& av);
static void build_project(std::vector<std::string>& av);

PragmaProcessor::PragmaProcessor(int argc, char *argv[])
{
	namespace CP = CommandlineProcessing;
	auto av = vector<string>(argv, argv + argc);
	if ((make_collector = CP::isOption(av, string("-build:"), string("collect"), true))) {
		collect_arguments(av);
		return;
	}
	int verbose = 0;
	indentation = 8;
	CP::isOptionWithParameter(av, string("-transformer:"),
					string("v|verbose"), verbose, true);
	compile = CP::isOption(av, string("-transformer:"),
					string("c|compile"), true);
	inline_all = CP::isOption(av, string("-transformer:"),
					string("i|inline"), true);
	profile = CP::isOption(av, string("-transformer:"),
					string("p|profile"), true);
	paralellize = CP::isOption(av, string("-paralellize:"),
					string("o|omp"), true);
	CP::isOptionWithParameter(av, string("-transformer:"),
				string("i|indentation"), indentation, true);
	l.set_min_level((log_level)verbose);
	if ((build_interception = CP::isOption(av, string("-build:"), string("make"), true))) {
		build_project(av);
		return;
	}
	project = frontend(av);
}

static void collect_arguments(std::vector<std::string>& av)
{
	auto project = frontend(av);
	std::ofstream files(BUILD_INT_FILENAME, std::ofstream::out | std::ofstream::app);
	for (auto file : project->get_files()) {
		files << file->getFileName() << std::endl;
	}
}

static void build_project(std::vector<std::string>& av)
{
	std::remove(BUILD_INT_FILENAME);
	namespace CP = CommandlineProcessing;
	std::string makefile_path = "Makefile";
	CP::isOptionWithParameter(av, string("-build:"), string("makefile"), makefile_path, true);
	std::string make_params;
	CP::isOptionWithParameter(av, string("-build:"), string("params"), make_params, true);
	std::stringstream orig_getter;
	orig_getter << "grep '^CFLAGS' " << makefile_path
		<< " | sed 's/CFLAGS *= *\\(.*\\)/\\1/' | sed 's/\\$/\\\\$/g'";
	auto orig_flags = exec(orig_getter.str().c_str());
	orig_flags.erase(std::remove(orig_flags.begin(), orig_flags.end(), '\n'), orig_flags.end());
	std::clog << "orig_flags: " << orig_flags << std::endl;
	std::stringstream orig_ldgetter;
	orig_ldgetter << "grep '^CLINKFLAGS' " << makefile_path << " | sed 's/CLINKFLAGS *= *\\(.*\\)/\\1/'";
	auto orig_ldflags = exec(orig_ldgetter.str().c_str());
	orig_ldflags.erase(std::remove(orig_ldflags.begin(), orig_ldflags.end(), '\n'), orig_ldflags.end());
	std::clog << "orig_ldflags: " << orig_ldflags << std::endl;

	// First collect files with code.
	std::stringstream collector;
	collector << "make " << make_params << " -f " << makefile_path
		<< " CC=\"" << av[0] << "\" CLINK=/bin/echo CFLAGS=\""
		<< orig_flags << " -build:collect\"" << std::endl;
	std::clog << "collector: " << collector.str() << std::endl;
	std::system(collector.str().c_str());
	// We don't call profiler here as it doesn't work anyways.
	// Now call the actual build with supplied params.
	int jobs = 1;
	CP::isOptionWithParameter(av, string("-build:"), string("j|jobs"), jobs, true);
	std::stringstream maker;
	maker << "make " << make_params << " -f " << makefile_path
		<< " CC=\"" << av[0] << "\" CFLAGS=\""
		<< orig_flags << " -fopenmp -paralellize:omp -transformer:c\" CLINKFLAGS=\""
		<< orig_ldflags << " -fopenmp\"" << " -j" << jobs << std::endl;
	std::system(maker.str().c_str());
}

namespace {
	// templates for pragma parser follow
	using Keyword = const char*;
	using KeywordV = std::vector<Keyword>;
	using Action = std::function<void(void)>;
	using ActionV = std::vector<Action>;

	/// A struct to match different types.
	/// Contains a function that does the work
	/// and is specialized to match different cases.
	template<typename T>
	struct mt_impl {
	static auto match()
	{
		static_assert(sizeof(T) == 0,
			"match_type not implemented for this type");
	}
	};

	template<typename T>
	auto match_type()
	{
		return mt_impl<std::remove_cv_t<T>>::match();
	}

	/// Match unsigned.
	template<>
	struct mt_impl<std::size_t> {
	static auto match()
	{
		int id;
		if (!afs_match_integer_const(&id) || (id < 0))
			throw runtime_error("Non-negative integer expected.");
		return static_cast<std::size_t>(id);
	}
	};

	/// Match variable name.
	template<>
	struct mt_impl<std::string> {
	static auto match()
	{
		if (!afs_match_identifier())
			throw std::string("An identifier expected, not found.");
		if (auto var = isSgVarRefExp(c_parsed_node)) {
			return get_var_name(var);
		} else if (auto fun = isSgFunctionRefExp(c_parsed_node)) {
			return fun->get_symbol()->get_name().getString();
		} else {
			auto fname = isSgName(c_parsed_node);
			if (!fname) {
				stringstream err;
				err << "An identifier of unknown type ";
				err << c_parsed_node->sage_class_name();
				err << " found. A variable or a function name expected.";
				throw err.str();
			}
			return fname->getString();
		}
	}
	};

	/// Match int.
	template<>
	struct mt_impl<int> {
	static auto match()
	{
		bool minus = false;
		if (afs_match_char('-'))
			minus = true;
		else
			afs_match_char('+');
		int shift;
		if (!afs_match_integer_const(&shift))
			throw runtime_error("integer expected");
		return minus ? -shift : shift;
	}
	};

	/// Match T0:T1 and make a pair.
	template<typename T0, typename T1>
	struct mt_impl<std::pair<T0, T1>> {
	static auto match()
	{
		auto i0 = match_type<T0>();
		if (!afs_match_char(':'))
			throw runtime_error("':' expected");
		auto i1 = match_type<T1>();
		return std::make_pair(i0, i1);
	}
	};

	/// Match T0:T1 or T0&T1 and make a 3-tuple.
	template<typename T0, typename T1>
	struct mt_impl<std::tuple<T0, T1, bool>> {
	static auto match()
	{
		auto i0 = match_type<T0>();
		bool check;
		if (afs_match_char(':'))
			check = true;
		else if (afs_match_char('&'))
			check = false;
		else
			throw runtime_error("':' or '&' expected");
		auto i1 = match_type<T1>();
		return std::make_tuple(i0, i1, check);
	}
	};

	/// Match T0:T1 or just T0 and make a 3-tuple where
	/// the second bool parameter determines if T1 was given.
	template<typename T0, typename T2>
	struct mt_impl<std::tuple<T0, bool, T2>> {
	static auto match()
	{
		auto i0 = match_type<T0>();
		if (!afs_match_char(':'))
			return std::make_tuple(i0, false, T2());
		auto i1 = match_type<T2>();
		return std::make_tuple(i0, true, i1);
	}
	};

	/// Match Direction.
	template<>
	struct mt_impl<Direction> {
	static auto match()
	{
		if (afs_match_char('<'))
			return Direction::straight;
		else if (afs_match_char('='))
			return Direction::none;
		else if (afs_match_char('>'))
			return Direction::backward;
		else if (afs_match_char('*'))
			return Direction::any;
		else
			throw runtime_error("expected one of {<, >, =, *}");
	}
	};

	/// Match actual parameters.
	template<typename T, bool list>
	struct DataMatcher {
	static auto match();
	};

	/// Match a single parameter of type T.
	template<typename T>
	struct DataMatcher<T, false> {
	static auto match()
	{
		return match_type<T>();
	}
	};

	/// Match a non-empty list: T[, T],
	/// like int[, int] or int&int, int:int, ...
	/// and put the matched data into ContainerT.
	template<typename ContainerT>
	struct DataMatcher<ContainerT, true> {
	static auto match()
	{
		ContainerT result;
		do {
			result.insert(result.end(),
				match_type<typename ContainerT::value_type>());
		} while (afs_match_char(','));
		return result;
	}
	};

	/// Match a probably optional parameter.
	/// Parameter can be a nonempty list or a single element.
	/// *d0* and *d1* determine the parentheses.
	template<typename ContainerT, bool list, bool optional,
		char d0, char d1>
	struct ParamMatcher {
	static auto match();
	};

	/// Non-optional specialization
	/// \todo fix error messages
	template<typename ContainerT, bool list, char d0, char d1>
	struct ParamMatcher<ContainerT, list, false, d0, d1> {
	static auto match()
	{
		if (!afs_match_char(d0))
			throw runtime_error(std::string(1, d0) + " expected");
		auto result = DataMatcher<ContainerT, list>::match();
		if (!afs_match_char(d1))
			throw runtime_error(std::string(1, d1) + " expected");
		return std::make_pair(true, result);
	}
	};

	/// An optional specialization.
	/// Return empty container if there are no parens.
	template<typename ContainerT, bool list, char d0, char d1>
	struct ParamMatcher<ContainerT, list, true, d0, d1> {
	static auto match()
	{
		if (!afs_match_char(d0)) {
			return std::make_pair(false, ContainerT());
		} else {
			auto r = DataMatcher<ContainerT, list>::match();
			if (!afs_match_char(d1))
				throw runtime_error(std::string(1, d1)
							+ " expected");
			return std::make_pair(true, r);
		}
	}
	};


	/// A wrapper for some of the functions above.
	/// If *optional* is set and no parameter list is present,
	/// *parsed is set to true.
	template<bool optional = false, bool list = true,
		char delim0 = '(', char delim1 = ')'>
	struct Parser {
		template<typename ContainerT>
		static void parse(ContainerT &dest, bool* parsed = nullptr);
	};

	/// Match a list
	template<bool opt, char d0, char d1>
	struct Parser<opt, true, d0, d1> {
		template<typename ContainerT>
		static void parse(ContainerT &dest, bool* parsed = nullptr)
		{
			using M = ParamMatcher<ContainerT, true, opt, d0, d1>;
			auto r = M::match();
			if (opt && !r.first && parsed)
				*parsed = true;
			std::move(r.second.begin(), r.second.end(),
					std::inserter(dest, dest.end()));
		}
	};

	/// Match a single param
	template<bool opt, char d0, char d1>
	struct Parser<opt, false, d0, d1> {
		template<typename T>
		static void parse(T &dest, bool* = nullptr)
		{
			auto r = ParamMatcher<T, false, opt, d0, d1>::match();
			if (r.first)
				dest = r.second;
		}
	};

template<bool analysis = false,
	typename Container = std::conditional_t<analysis, AnalysisParams,
							TransformParams>>
auto parse_pragma();

/// Parses all supported transforming pragmas:
/// `#pragma x ...orders...`
///
/// \todo allow empty local pragma to enforce all variables
/// to be propagated locally.
template<>
auto parse_pragma<false>()
{
	TransformParams p;
	assert(afs_match_substr(TF_KWD));
	KeywordV keywords = {
		"index", "nontemp", "partition", "id",
		"unroll", "permut", "merge", "swap",
		"map", "propagate", "local", "dead",
		"split", "bringout", "inline", "distribute",
		"paralellize"
	};

	ActionV actions = {
		[&p] { Parser<>::parse(p.index_shift_vars); }, // index
		[&p] { p.remove = false; }, // nontemp
		[&p] { p.border = true; }, // partition
		[&p] { Parser<false, false>::parse(p.id); }, // id
		[&p] { Parser<true>::parse(p.unroll_vars, &p.unroll); },//unroll
		[&p] { Parser<>::parse(p.permutation_indices); }, // permut
		[&p] {
			auto s = p.merge_indices.size();
			p.merge_indices.resize(s + 1);
			Parser<>::parse(p.merge_indices[s]);
		}, // merge
		[&p] { Parser<>::parse(p.swap_indices); }, // swap
		[&p] { // map
			Parser<>::parse(p.mapping_vars);
			auto rng = p.mapping_vars | boost::adaptors::map_values;
			p.propagate_vars.insert(rng.begin(), rng.end());
		},
		[&p] { Parser<>::parse(p.propagate_vars); }, // propagate
		[&p] { Parser<>::parse(p.propagate_locally); }, // local
		[&p] { Parser<>::parse(p.local_variables); }, // dead
		[&p] { Parser<true>::parse(p.split_vars, &p.partition);},//split
		[&p] { Parser<true>::parse(p.bringout_vars,
					&p.bringout_everything); }, // bringout
		[&p] {
			Parser<true, false, '[', ']'>::parse(p.idepth);
			Parser<true>::parse(p.functions_to_inline,
					&p.inline_everything);
		}, // inline
		[&p] { p.distribute = true; }, // distribute
		[&p] { p.paralellize = true; }, // paralellize
	};
	assert(actions.size() == keywords.size());

	while (*c_char) {
		auto cur_k = std::find_if(keywords.begin(), keywords.end(),
				[] (auto k) { return afs_match_substr(k, false); });
		if (cur_k == keywords.end())
			throw runtime_error("Trailing characters.");
		else
			actions[std::distance(keywords.begin(), cur_k)]();
	}
	return p;
}

/// Parses all supported analysis pragmas:
/// `#pragma a ...orders...`
///
/// TODO merge algorithmic part with the function above,
/// pass the containers to it.
template<>
auto parse_pragma<true>()
{
	AnalysisParams p;
	assert(afs_match_substr(AN_KWD));
	KeywordV keywords = {
		"reduction",
		"dependency",
		"has_dependency_vector",
	};

	ActionV actions = {
		[&p] { p.reduction = true; }, // reduction
		[&p] { p.dependency = true; }, // dependency
		[&p] { Parser<>::parse(p.dependency_vector); }, // set dependencies
	};
	assert(actions.size() == keywords.size());

	while (*c_char) {
		auto cur_k = std::find_if(keywords.begin(), keywords.end(),
				[] (auto k) { return afs_match_substr(k, false); });
		if (cur_k == keywords.end())
			throw runtime_error("Trailing characters.");
		else
			actions[std::distance(keywords.begin(), cur_k)]();
	}
	return p;
}
} // namespace

namespace {
/// A wrapper for either *CodeTransformer* or *CodeAnalyser*
/// that passes in the parameters and handles exceptions when they
/// are thrown.
template<bool analysis,
	typename Container,
	typename Handler = std::conditional_t<analysis, CodeAnalyser,
							CodeTransformer>>
void try_to_handle_stmt(SgStatement* stmt, Container&& p)
{
	try {
		Handler handler(stmt, std::move(p));
		handler.handle_block();
	} catch (const char *s) {
		l << LOG_ERR << stmt;
		l << "Couldn't handle block: " << s << endl;
	} catch (const string &s) {
		l << LOG_ERR << stmt;
		l << "Couldn't handle block: " << s << endl;
	} catch (const Exception &e) {
		l << LOG_ERR << e;
	} catch(...) {
		l << LOG_ERR << stmt;
		l << "couldn't handle block: caught exception" << endl;
	}
}

/// Tries to parse *p* into *params*. Handles errors.
template<bool analysis = false,
	typename Container = std::conditional_t<analysis, AnalysisParams,
							TransformParams>>
bool parse_wrapper(SgPragmaDeclaration* p, Container& params)
{
	auto pragma = p->get_pragma()->get_pragma();
	try {
		c_char = pragma.c_str();
		c_sgnode = p;
		params = parse_pragma<analysis>();
	} catch (const std::exception &e) {
		auto str = e.what();
		auto file_pos = p->get_startOfConstruct();
		string full_name = file_pos->get_filenameString();
		full_name.replace(0, 1 + full_name.rfind("/"), "");
		l << LOG_ERR << "<" << full_name << " @ ";
		l << file_pos->get_line() << ":";
		l << file_pos->get_col() + (pragma.c_str() - c_char);
		l << "> " << str << endl;
		return false;
	} catch (...) {
		l << LOG_ERR << p;
		l << "error parsing pragma" << endl;
		return false;
	}
	return true;
}

template<bool analysis = false>
void try_to_handle_pragma(SgPragmaDeclaration* p);

template<>
void try_to_handle_pragma<true>(SgPragmaDeclaration* p)
{
	AnalysisParams params;
	if (!parse_wrapper<true>(p, params))
		return;

	auto stmt_to_analyse = getNextStatement(p);
	if (!stmt_to_analyse) {
		l << LOG_ERR << p;
		l << "Pragma not after statement." << endl;
		return;
	}
	if (!params.dependency_vector.empty() && !isSgForStatement(stmt_to_analyse)) {
		l << LOG_ERR << p;
		l << "Pragma dependency_vector not after a for loop." << endl;
		return;
	}
	// call the analysis
	try_to_handle_stmt<true>(stmt_to_analyse, std::move(params));
	// remove pragma
	removeStatement(p);
	deepDelete(p);
}

template<>
void try_to_handle_pragma<false>(SgPragmaDeclaration* p)
{
	TransformParams params;
	if (!parse_wrapper<false>(p, params))
		return;

	auto stmt_to_transform = getNextStatement(p);
	if (!stmt_to_transform) {
		l << LOG_ERR << p;
		l << "Pragma not after statement" << endl;
		return;
	}
	if (params.paralellize) {
		// paralellization requested from a pragma.
		Paralellizer paralellizer(stmt_to_transform);
		paralellizer.run();
		return;
	}
	// the default value for params.remove is true so that we
	// can reflect a nontemp pragma when it's found,
	// but for non-blocks we set the default value here and it's false.
	if (!isSgBasicBlock(stmt_to_transform))
		params.remove = false;
	// if an id is given for basic block, do not remove it
	if (isSgBasicBlock(stmt_to_transform)
			&& (params.id != -1))
		params.remove = false;
	// transform the statement
	try_to_handle_stmt<false>(stmt_to_transform, std::move(params));
	// remove pragma
	removeStatement(p);
	deepDelete(p);
}
} // namespace

namespace {
/// Find main() and inline everything in it.
bool global_inlining()
{
	auto main = findMain(getProject());
	if (!main) {
		l << LOG_ERR;
		l << "Global inliner: couldn't find main(). Provide all the required files on the commandline.";
		l << endl;
		return false;
	}
	try {
		inline_calls(main, TfUtils::STRV(), IDEPTH);
	} catch (Exception& e) {
		l << LOG_ERR << e;
		return false;
	} catch (...) {
		l << LOG_ERR << "Unknown error while inlining. This is probably a bug." << endl;
		return false;
	}
	return true;
}
} // namespace


/// The handler.
///
/// Works as follows:
/// * Does the things requested on commandline (i.e. global inlining)
/// * Selects all pragmas.
/// * Iterates the resulting vector in reverse direction and for
///   each pragma tries to call the appropriate handler.
void PragmaProcessor::process()
{
	using namespace boost::adaptors;
	if (inline_all && !global_inlining())
		return;
	if (profile) {
		Profiler p(getProject());
		p.profile();
		p.dump();
	}

	auto pragmas = querySubTree<SgPragmaDeclaration>(project);
	for (auto p : pragmas | reversed) {
		auto kwd = extractPragmaKeyword(p);
		if (kwd == TF_KWD) {
			l << p << " Transformation pragma found.";
			l << endl;
			try_to_handle_pragma<false>(p);
		} else if (kwd == AN_KWD) {
			l << p << " Analysis pragma found." << endl;
			try_to_handle_pragma<true>(p);
		} else {
			continue; // not our pragma
		}
	}

	if (paralellize) {
		Paralellizer p(getProject());
		p.run();
	}
}


/// Unparses the file or calls the compiler based
/// on settings provided on construction.
///
/// In case of unparsing and errors returns 23.
int PragmaProcessor::backend()
{
	MintConstantFolding::constantFoldingOptimization(project);
	// AstTests::runAllTests(project);
	if (compile) {
		return ::backend(project);
	} else {
		if (project->numberOfFiles() == 1) {
			// using new here as unparse calls delete itself.
			UnparseFormatHelp* f = new UnparseFormatCustom(indentation);
			project->unparse(f);
		} else {
			project->unparse();
		}
	}
	return (l.get_errors() ? 23 : 0);
}


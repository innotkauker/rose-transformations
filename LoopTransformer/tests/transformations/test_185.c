#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}

	double c[] = {1,2,3,4};

	int j;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, y1 = 0;
	double maxc = c[0];
	double minc = c[0];
	double maxa = a[0][0];
	double mina = a[0][0];
	#pragma x distribute
	for (unsigned i = 0; i < m; i++) {
		maxc = (c[i] > maxc ? c[i] : maxc);
		minc = (minc < c[i] ? minc : c[i]);
		for (int j = 0; j < n; j++)
			maxa = (maxa > a[i][j] ? maxa : a[i][j]);
		for (int j = 0; j < n; j++)
			mina = (a[i][j] < mina ? a[i][j] : mina);
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}

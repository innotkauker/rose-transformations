#include <stdio.h>

int f(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	return r;
}

void g(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	printf("%i\n", r);
}

int id(int x)
{
	return x;
}

int main()
{
	int a = 5;
	int r = 6;
	double b = 0.42;
	double c = 5e2;
	#pragma x inline
	switch (id(r)) {
	case 0:
		printf("false\n");
		break;
	case 6:
		printf("true\n");
		break;
	default:
		printf("default\n");
	}
	#pragma x inline
	switch (a) {
	case 0:
		g(0);
		break;
	case 5:
		g(5);
		break;
	default:
		printf("default\n");
	}
}


#!/usr/bin/env bash
exec 2> /dev/null

home="../../../"
test=$1;
rm -rf wd/$test
mkdir -p wd/$test
cd wd/$test;

exec_log="exec.log";
test_path="filtered_$test";
task_path="task_$test";
translator_path="../../../../translator";
translator_params = "--edg:no_warnings";
log_path="$test.log"
old_res="$test.old.log"
new_res="$test.new.log"
filtered_log="$test.filtered.log"
filtered_result="$test.filtered.result"
test_bin="$test.o"
transformed_test="rose_${test_path}"
transformed_test_bin="rose_${test_path}.o"

grep '^//+' ../../$test > $task_path;
grep -v '^//+' ../../$test > $test_path;

cleanup() {
	echo >/dev/null
	# rm ${log_path};
	# rm ${test_bin};
	# rm $transformed_test_bin;
	# rm $transformed_test;
	# rm ${task_path};
	# rm ${test_path};
	# rm -f ${rose_test}.o;
	# rm -f ${old_res};
	# rm -f ${new_res};
	# rm -f $filtered_log;
	# rm -f $filtered_result;
}

echo -n "$test ";

gcc -std=c99 -g -o $test_bin $test_path &> /dev/null;
cc_code=$?
if [ $cc_code -ne 0 ]; then
	echo -e "\t\033[31m[BAD TEST]\033[0m";
	cleanup;
	exit;
fi;

$($translator_path $translator_params $test_path &> $log_path);
tr_code=$?
if [[ $tr_code != 0 && $tr_code != 23 ]]; then # 23 means transformation error
	echo -e "\t\033[35m[ABORTED WITH CODE=$tr_code]\033[0m";
	cleanup;
	exit;
fi;

if [ "$(grep '^//+X' $task_path)" != "" ]; then
	# transformations may have occured
	# check results equivalence
	timeout 1 ./$test_bin > $old_res;
	orig_code=$?
	if [ $orig_code -eq 124 ]; then
		echo -e "\t\033[36m[TIMED OUT / TEST]\033[0m";
		cleanup;
		exit;
	elif [ $orig_code -ne 0 ]; then
		echo -e "\t\033[31m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
	gcc -std=c99 -g -o $transformed_test_bin $transformed_test &>> $log_path;
	timeout 1 ./$transformed_test_bin > $new_res 2> /dev/null;
	res_code=$?
	if [ $res_code -eq 124 ]; then
		echo -e "\t\033[36m[TIMED OUT / RESULT]\033[0m";
		cleanup;
		exit;
	elif [ $res_code -ne 0 ]; then
		echo -e "\t\033[31m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
	diff -up $old_res $new_res
	if [ "$(diff -up $old_res $new_res)" != "" ]; then
		echo -en "\t\033[32m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
fi;

function empty_filter
{
	cat $1
}

function dependency_analysis_filter
{
	cat $1 | grep '^\[DEBUG\]: D' | grep -v -- '---' \
		| grep -v 'index.*direction.*distance.*iterations' \
		| sed 's/[^|]*| \(.*\)/\1/' | sed 's/[ \t]\+/ /g' \
		| ruby "$home/merger.rb" | sed 'N;s/\n/ /' \
		| sed 's/[^@]* @ \([^:]*\):[^@]* @ \([^:]*\):[^>]*> \(.*\)/\1 \2\3/'
}

FILTER=empty_filter

if [ "$(head -n1 $task_path | grep '^//+C')" != "" ]; then
	# global config present - can affect behavior
	if [ "$(head -n1 $task_path | grep '^//+.A')" != "" ]; then
		# filter the target with a set of commands - used in dep analysis.
		FILTER=dependency_analysis_filter
	fi;
	# TODO the number of filtered lines must match that of tests
fi;

echo Filter is $FILTER >> $exec_log
eval $FILTER "$log_path" > $filtered_log;
eval $FILTER "rose_${test_path}" > $filtered_result;

total=0;
passed=0;
from_log=0;
from_result=0;
while read tst; do
	t='undefined';
	if [ "$(echo $tst | grep '^//+T')" != "" ]; then
		t='match';
	elif [ "$(echo $tst | grep '^//+F')" != "" ]; then
		t='not match';
	fi;
	if [ "$t" == "undefined" ]; then
		continue;
	fi;

	(( total = $total + 1 ));

	# match positive/false positive
	if [ "$(echo $tst | grep '^//+.R')" != "" ]; then
		grep_mode='';
	else
		grep_mode='-F';
	fi;

	# scan the log
	if [ "$(echo $tst | grep '^//+.L')" != "" ]; then
		grep_mode='';
		scan_log=1;
		target=$filtered_log;
		(( from_log = $from_log + 1 ));
	else
		target=$filtered_result;
		(( from_result = $from_result + 1 ));
	fi;

	tst=$(echo $tst | sed "s/\/\/+[^ ]* \(.*\)/\1/");
	if [ "$(grep $grep_mode "$tst" "$target")" == "" ]; then
		if [ "$t" == "not match" ]; then
			(( passed = $passed + 1 ));
		else
			echo "$tst - FALSE POSITIVE" >> $exec_log;
		fi;
	else
		if [ "$t" == "match" ]; then
			(( passed = $passed + 1 ));
		else
			echo "$tst - FALSE NEGATIVE" >> $exec_log;
		fi;
	fi;
done < $task_path;

if [ "$from_result" != "0" && "$(wc -l $filtered_result | sed 's/\([^ ]*\) .*/\1/')" != "$from_result" ]; then
	(( total = total + 1 ));
fi;
if [ "$from_log" != "0" && "$(wc -l $filtered_log | sed 's/\([^ ]*\) .*/\1/')" != "$from_log" ]; then
	(( total = total + 1 ));
fi;

cleanup;

echo -en "\t[$passed/$total]";

if (( total == passed )); then
	echo -en "\t\033[32m[PASSED]\033[0m";
else
	echo -en "\t\033[31m[FAILED]\033[0m";
fi;
if [ "x$(grep WARNING $log_path)" != "x" ]; then
	echo -en "\t\033[33m[W]\033[0m";
else
	echo -en "\t   ";
fi;
if [ "x$(grep ERROR $log_path)" != "x" ]; then
	echo -en "\033[31m[E]\033[0m";
fi;
echo;


#define BOOST_RESULT_OF_USE_DECLTYPE
#include "StatementTransformations.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <deque>
#include <regex>
#include <algorithm>
#include <iterator>
#include <pwd.h>
#include "sage3basic.h"
#include "inliner.h"
#include "inlinerSupport.h"
#include "CodeTransformer.h"
#include "util.h"
#include "MintConstantFolding.h"
#include "LoopTransformations.h"
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/irange.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/range/iterator_range.hpp>

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace AstFromString;
using namespace LoopTransformations;
using namespace TfUtils;
using namespace SPRAY;

typedef pair<SgExpression*, SgExpression*> MappingRule;

BlockTraveller::BlockTraveller(SgStatement *target, BTParams&& params,
						bool log/* = true*/)
	: p(std::move(params)), target(target), log(log),
	rd(getEnclosingNode<SgFunctionDefinition>(target, true))
{
	using namespace boost::adaptors;
	using st = std::map<std::string, std::size_t>::value_type;
	boost::copy(p.propagate | transformed([] (const std::string& v) {
					return st(v, 0);
				}), inserter(p_stats, p_stats.end()));
}

// ---------> Type alias
namespace {
using MappedVarsT = std::map<const SgInitializedName*, lref>;
using MappedArraysT = std::map<const SgPntrArrRefExp*, lref>;
}

// ---------> Mapping

namespace {

/// Replaces ++, --, +=, -=, *=, /= found in *expr_to_extend*
/// with corresponding assignments.
/// \todo replace if-else with a switch?
bool convert_modifier_to_assignment(SgExpression *expr_to_extend)
{
	bool transformed = true;
	if (isSgAssignOp(expr_to_extend)) {
		transformed = false;
	} if (auto plus_plus = isSgPlusPlusOp(expr_to_extend)) {
		auto rhs = plus_plus->get_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(rhs),
				buildAddOp(deepCopy(rhs), buildIntVal(1)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto minus_minus = isSgMinusMinusOp(expr_to_extend)) {
		auto rhs = minus_minus->get_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(rhs),
				buildSubtractOp(deepCopy(rhs), buildIntVal(1)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto plus_assign = isSgPlusAssignOp(expr_to_extend)) {
		auto rhs = plus_assign->get_rhs_operand();
		auto lhs = plus_assign->get_lhs_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(lhs),
				buildAddOp(deepCopy(lhs), deepCopy(rhs)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto minus_assign = isSgMinusAssignOp(expr_to_extend)) {
		auto rhs = minus_assign->get_rhs_operand();
		auto lhs = minus_assign->get_lhs_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(lhs),
				buildSubtractOp(deepCopy(lhs), deepCopy(rhs)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto times_assign = isSgMultAssignOp(expr_to_extend)) {
		auto rhs = times_assign->get_rhs_operand();
		auto lhs = times_assign->get_lhs_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(lhs),
				buildMultiplyOp(deepCopy(lhs), deepCopy(rhs)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto divide_assign = isSgDivAssignOp(expr_to_extend)) {
		auto rhs = divide_assign->get_rhs_operand();
		auto lhs = divide_assign->get_lhs_operand();
		SgExpression *new_expr = buildAssignOp(deepCopy(lhs),
				buildDivideOp(deepCopy(lhs), deepCopy(rhs)));
		replaceExpression(expr_to_extend, new_expr);
	} else if (auto gen_assign = isSgCompoundAssignOp(expr_to_extend)) {
		l << LOG_WARN << gen_assign;
		l << "found a non-supported modifying operator, skipping";
		l << endl;
		transformed = false;
	}
	return transformed;
}

/// Checks if *rhs* can be used to make a mapping rule for *map_to*.
///
/// Check fails if *rhs* contains:
/// * more or less then one reference to *map_to*
/// * references to *map_from*
/// * any modifiers
bool mapping_is_possible(const SgExpression *rhs,
			const string &map_to, const string &map_from)
{
	auto rhs_refs = querySubTree<SgVarRefExp>(rhs);
	int refs_n = count_if(rhs_refs.begin(), rhs_refs.end(),
				[&map_to] (const SgVarRefExp *v) {
					return get_var_name(v) == map_to;
				});
	if (refs_n > 1) {
		l << LOG_WARN << rhs;
		l << "Unable to use assignment for mapping as its rhs contains more than one reference to ";
		l << map_to << endl;
		return false;
	} else if (refs_n == 0) {
		l << rhs;
		l << "Unable to use assignment for mapping as its rhs contains no reference to ";
		l << map_to << endl;
		return false;
	}

	if (find_if(rhs_refs.begin(), rhs_refs.end(),
				[&map_from] (const SgVarRefExp *v) {
					return get_var_name(v) == map_from;
				}) != rhs_refs.end()) {
		l << LOG_WARN << rhs;
		l << "unable to use assignment for mapping as its rhs contains reference to ";
		l << map_from << endl;
		return false;
	}
	CNS modified_vars;
	get_modified_variables(rhs, modified_vars);
	if (!modified_vars.empty()) {
		l << LOG_WARN << rhs;
		l << "unable to use assignment for mapping as its rhs contains modifiers" << endl;
		return false;
	}
	return true;
}
} // namespace

/// Tries to get a scheme for mapping from *statement*
/// using get_mapping_rule()
///
/// Puts found rules into *mapped_vars* or
/// *mapped_arrays* for further propagation.
bool BlockTraveller::try_to_use_for_mapping(SgExprStatement *statement)
{
	SgExpression *lhs = nullptr, *rhs = nullptr;
	isAssignmentStatement(statement, &lhs, &rhs);
	assert(lhs);
	assert(rhs);
	assert(isSgAssignOp(statement->get_expression()));
	string var_name = get_var_name(lhs);
	string map_to = p.mapping.find(var_name)->second;

	if (!mapping_is_possible(rhs, map_to, var_name))
		return false;

	propagate_omitting_top_var(lhs, statement);
	MappingRule mapping_rule(deepCopy(lhs), deepCopy(rhs));
	auto res = express_var_in_rhs(mapping_rule.first, mapping_rule.second,
					map_to);
	swap(mapping_rule.first, mapping_rule.second);
	// we didn't propagate into rhs before as mapping rule would be broken
	propagate_selected_variables(rhs, statement);
	if (!res) {
		// couldn't calculate mapping rule
		l << LOG_WARN << statement;
		l << ": couldn't find SgVarRefExp or SgPntrArrRefExp referencing ";
		l << var_name << "; unable to use the statement for mapping.";
		l << "Assignment to " << var_name << " will be left as is" << endl;
		return false;
	}
	l << "Assignment to " << var_name;
	l << ", which will be used for mapping: ";
	l << mapping_rule.first->unparseToString() << " -> ";
	l << mapping_rule.second->unparseToString() << endl;

	// insert the calculated mapping into the containers
	if (auto arr_ref = isSgPntrArrRefExp(mapping_rule.first)) {
		typedef MappedArraysT::value_type atype;
		auto mapping = atype(deepCopy(arr_ref),
					lref(mapping_rule.second, statement));
		mapped_arrays.insert(mapping);
	} else if (auto var_ref = isSgVarRefExp(mapping_rule.first)) {
		typedef MappedVarsT::value_type vtype;
		auto mapping = vtype(get_var_initialized_name(var_ref),
					lref(mapping_rule.second, statement));
		mapped_vars.insert(mapping);
	} else {
		assert(0);
	}
	return true;
}

// --------> Propagation

/// Propagates variables into *lhs* treating it as the left part
/// of assignment at *statement* and omitting top variable reference in it.
void BlockTraveller::propagate_omitting_top_var(SgExpression *lhs,
						SgStatement *statement)
{
	assert(lhs);
	assert(statement);
	if (isSgVarRefExp(lhs))
		return;
	EV subscripts;
	auto ptr_subscripts = &subscripts;
	if (isArrayReference(lhs, nullptr, &ptr_subscripts)) {
		for (auto s : subscripts)
			propagate_selected_variables(s, statement);
		return;
	}
	propagate_selected_variables(lhs, statement);
}

/// Propagates variables into *statement*.
/// Converts compound modifiers to assignments,
/// uses propagate_omitting_top_var() if lhs can be found.
void BlockTraveller::propagate_into_expr_statement(SgExprStatement *statement)
{
	SgExpression *lhs;
	SgExpression *rhs;
	auto e = statement->get_expression();
	if (isSgPlusPlusOp(e) || isSgMinusOp(e)) {
		lhs = get_unary_modifier_expression(e);
		rhs = nullptr;
	} else if (!isAssignmentStatement(statement, &lhs, &rhs)) {
		propagate_selected_variables(statement->get_expression(),
						statement);
		return;
	}

	if (get_var_initialized_name(lhs) && p.propagate.count(get_var_name_from_node(lhs))) {
		convert_modifier_to_assignment(statement->get_expression());
		assert(isAssignmentStatement(statement, &lhs, &rhs));
	}
	if (rhs)
		propagate_selected_variables(rhs, statement);
	propagate_omitting_top_var(lhs, statement);
}

// ---------> Handlers

/// A handler for one-expression statements.
///
void BlockTraveller::handle_expr_statement(SgExprStatement *statement)
{
	auto orig_stmt = statement->unparseToString();
	auto expression = statement->get_expression();
	auto assignment = isSgAssignOp(expression);
	if (!assignment) {
		propagate_into_expr_statement(statement);
		output_statement_alteration(orig_stmt, statement);
		return;
	}
	auto lhs = assignment->get_lhs_operand_i();
	if (!isSgVarRefExp(lhs) && !isSgPntrArrRefExp(lhs)) {
		propagate_selected_variables(expression, statement);
		output_statement_alteration(orig_stmt, statement);
		return;
	}

	string var_name = get_var_name(lhs);
	if (p.mapping.count(var_name) && try_to_use_for_mapping(statement)) {
		output_statement_alteration(orig_stmt, statement);
		return;
	}

	propagate_into_expr_statement(statement);
	l << "assignment to " << var_name;
	l << " will be left as is" << endl;
	output_statement_alteration(orig_stmt, statement);
}

/// A handler for complex constructs, like blocks, loops, switches etc.
///
/// \todo Replace if-else with a switch.
void BlockTraveller::handle_scope_statement(SgStatement *stmt)
{
	if (auto switch_block = isSgSwitchStatement(stmt)) {
		dispatch_stmt(switch_block->get_item_selector());
		dispatch_stmt(switch_block->get_body());
	} else if (auto case_option = isSgCaseOptionStmt(stmt)) {
		dispatch_stmt(case_option->get_body());
	} else if (auto default_option = isSgDefaultOptionStmt(stmt)) {
		dispatch_stmt(default_option->get_body());
	} else if (auto basic_block = isSgBasicBlock(stmt)) {
		auto statements = basic_block->get_statements();
		for (auto i : statements)
			dispatch_stmt(i);
	} if (auto do_while = isSgDoWhileStmt(stmt)) {
		dispatch_stmt(do_while->get_body());
		dispatch_stmt(do_while->get_condition());
	} if (auto for_block = isSgForStatement(stmt)) {
		auto inits = for_block->get_for_init_stmt()->get_init_stmt();
		for (auto i : inits)
			dispatch_stmt(i);
		dispatch_stmt(for_block->get_test());
		/// \todo fix loop increment dispatchment
		// dispatch_stmt(for_block->get_increment(), true, false);
		dispatch_stmt(for_block->get_loop_body());
	} if (auto if_block = isSgIfStmt(stmt)) {
		dispatch_stmt(if_block->get_true_body());
		dispatch_stmt(if_block->get_false_body());
		dispatch_stmt(if_block->get_conditional());
	} if (auto while_block = isSgWhileStmt(stmt)) {
		dispatch_stmt(while_block->get_body());
		dispatch_stmt(while_block->get_condition());
	} else {
		/// \todo check what other scope statements can be here
		// l << LOG_WARN << stmt << "unknown construct" << endl;
	}
}

/// A separate handler for variable declarations.
///
void BlockTraveller::handle_variable_declaration(SgVariableDeclaration *stmt)
{
	string initial_stmt_str = stmt->unparseToString();
	auto var = get_declared_var(stmt);
	auto gen_init = var->get_initializer();
	if (auto assign_init = isSgAssignInitializer(gen_init)) {
		auto rhs = assign_init->get_operand();
		propagate_selected_variables(rhs, stmt);
	} else if (auto aggregate_init = isSgAggregateInitializer(gen_init)) {
		auto initializers = aggregate_init->get_initializers()
							->get_expressions();
		for (auto i : initializers)
			propagate_selected_variables(i, stmt);
	} else if (gen_init) {
		l << LOG_WARN << stmt << "unsupported initializer of ";
		l << var << endl;
	}
	output_statement_alteration(initial_stmt_str, stmt);
}

/// Calls handlers based on *stmt* type.
///
void BlockTraveller::dispatch_stmt(SgStatement *stmt)
{
	/// \todo handle SgCastExp
	if (isSgScopeStatement(stmt) || isSgCaseOptionStmt(stmt) ||
			isSgDefaultOptionStmt(stmt)) {
		handle_scope_statement(stmt);
	} else if (auto estmt = isSgExprStatement(stmt)) {
		handle_expr_statement(estmt);
	} else if (auto var_decl = isSgVariableDeclaration(stmt)) {
		handle_variable_declaration(var_decl);
	} else if (stmt) {
		l << stmt << "unhandled statement of class ";
		l << stmt->class_name() << endl;
	}
}

/// Gets reaching definitions for all references in *refs*
/// (which should be children of *stmt*) which have them.
///
/// First tries to find mapping expressions, then
/// asks get_reaching_definitions() for the result
/// and selects the appropriate one.
///
reaching_defs BlockTraveller::get_definitions(SgStatement *stmt, ES&& refs)
{
	using namespace boost::adaptors;
	reaching_defs result;
	for (auto it = refs.begin(); it != refs.end(); ) {
		lref reaching_definition;
		if (find_mapping_definition(*it, reaching_definition)) {
			auto def = make_pair(*it, reaching_definition);
			result.insert(def);
			it = refs.erase(it);
		} else {
			it++;
		}
	}
	rd_query query; // provide information about propagation locality
	auto& pp = p;
	boost::copy(refs | transformed([&pp] (auto e) {
			auto n = get_var_initialized_name(e)->get_name();
			auto pos = pp.local.find(n);
			bool local_propagation = (pos != pp.local.end());
			return std::make_pair(local_propagation, e);
		}),
		std::back_inserter(query));
	rd.get_definitions(stmt, query, result);
	return result;
}

/// Replaces references to variables from *p.propagate*
/// with their reaching definitions when possible.
///
/// Works as follows:
/// * gets all references in *expr_to_extend*
/// * removes those were not requested for propagation
/// * for each left gets a reaching definition
///   first from mapping, then from framework analysis
/// * the definition is propagated, reaching definitions
///   are re-calculated
/// * legitimity of the propagation is checked
///   and it is rolled back if the check failed
void BlockTraveller::propagate_selected_variables(SgExpression *expr_to_extend,
					SgStatement* enc_stmt)
{
	assert(expr_to_extend);
	assert(enc_stmt);
	ES references;
	get_all_references(expr_to_extend, references);
	erase_if(references, [this] (SgExpression *e) {
			if (!get_var_initialized_name(e))
				return true;
			auto name = get_var_initialized_name(e)->get_name();
			if (p.propagate.find(name) == p.propagate.end())
				return true;
			else if (is_modified(e))
				return true;
			else
				return false;
		});

	/// \todo when fixing inlining, no rdefs are generated :(
	auto cur_rdefs = get_definitions(enc_stmt, std::move(references));
	//if (cur_rdefs.empty())
	//        return;
	for (auto r : cur_rdefs) {
		auto ref = r.first;
		lref rdef = r.second;
		SgExpression* propagated_reference = deepCopy(rdef.first);
		replaceExpression(ref, propagated_reference, true);
		if (!rdef.second) {
			p_stats[get_var_initialized_name(ref)->get_name()]++;
			// the value is from mapping, do not check
			// \todo probably check even when mapping is performed?
			continue;
		}
		lref pdef(propagated_reference, enc_stmt);
		if (!rd.propagation_was_correct(rdef, pdef)) {
			replaceExpression(propagated_reference, ref);
			l << LOG_WARN << ref;
			l << "unable to propagate the definition of ";
			l << get_var_initialized_name(ref);
			l << " as the variables it depends on have chenged";
			l << endl;
		} else {
			p_stats[get_var_initialized_name(ref)->get_name()]++;
		}
	}
}

/// Tries to find a saved definition for *ref*.
///
/// Looks in *mapped_vars* and *mapped_arrays*.
/// If found, puts it into *mapping_definition* and returns true,
/// otherwise returns false.
bool BlockTraveller::find_mapping_definition(const SgExpression *ref,
					lref &mapping_definition)
{
	if (auto var = isSgVarRefExp(ref)) {
		try {
			auto name = get_var_initialized_name(var);
			mapping_definition = mapped_vars.at(name);
		} catch (...) {
			return false;
		}
	} else if (auto arr = isSgPntrArrRefExp(ref)) {
		CEV subexpressions;
		query_subtree<SgExpression>(arr, subexpressions);
		RefFinder finder(&subexpressions);
		auto pos = find_if(mapped_arrays.begin(),
					mapped_arrays.end(), finder);
		if (pos == mapped_arrays.end())
			return false;
		mapping_definition = pos->second;
	} else {
		assert(0);
	}
	// This is a workaround to tell that the value is from mapping
	// so that we will not check its correctness later.
	/// \todo Probably fix this so that the check could still be used.
	mapping_definition.second = nullptr;
	return true;
}

/*
/// Removes variable declaration, tries to handle name tables appropriately
///
/// \todo fix tree check errors here?
void BlockTraveller::remove_variable_declaration(SgVariableDeclaration *declaration)
{
	auto symbol_table = getEnclosingNode<SgScopeStatement>(declaration)->get_symbol_table();
	// symbol_table->print();
	auto init_name = declaration->get_variables()[0];
	auto name = symbol_table->find(init_name);
	if (!name) {
		l << "no name corresponding to " << declaration->unparseToString();
		l << "was found in symbol table" << endl;
	}
	l << "removing declaration of " << init_name << endl;
	symbol_table->remove(name);
	removeStatement(declaration);
	deepDelete(declaration);
}
*/

/// We can't remove anything when there is a statement that modifies
/// more than one variable.
///
/// \todo because... ?
bool BlockTraveller::removal_possible()
{
	auto statements = split_block_into_statements(target);
	auto modified_vars = get_vars_modified_in_statements(statements);
	for (auto names : modified_vars)
		if (names.size() >= 2)
			return false;
	return true;
}

/// Removes assignments to variables that have been fully propagated.
///
/// For each statement in block gets its modified vars, removes those
/// LV analysis considers dead and deletes the statement
/// if nothing is left.
///
/// \todo check if the LV graph must be re-calculated based on statement insertion?
void BlockTraveller::remove_unused_assignments(bool remove_everything)
{
	if (!remove_everything)
		return;
	auto encl_fn = getEnclosingFunctionDefinition(target);
	unique_ptr<LVAnalysis> lv_analysis = perform_liveness_analysis(encl_fn);

	auto statements = split_block_into_statements(target);
	auto modified_vars = get_vars_modified_in_statements(statements);
	//boost::for_each(modified_vars, [] (auto& s) { l << "-> " << &s << endl; });
	for (unsigned i = 0; i < statements.size(); i++) {
		auto s = statements[i];
		if (modified_vars[i].empty())
			continue;
		set<string> live_vars;
		get_live_variables(lv_analysis.get(), s, &live_vars, 0);
		RefFinder filter(&live_vars, true);
		erase_if(modified_vars[i], filter);
		if (modified_vars[i].empty()) {
			// modifying only vars that won't be used later
			l << "removing statement " << s->unparseToString();
			l << " with ";
			l << modified_vars[i].size() << " modified vars" << endl;
			remove_statement(s);
			/// \todo why does this fail?
			// deepDelete(s);
		}
	}
}

/// Perform variable propagation and mapping
/// for the current block.
void BlockTraveller::traverse_block()
{
	assert(target);
	rd.regenerate();

	if (auto expr = isSgExprStatement(target)) {
		// propagating into a single statement
		dispatch_stmt(expr);
	} else if (isSgScopeStatement(target)) {
		// propagating into all block statements
		for (auto statement : querySubTree<SgStatement>(target)) {
			if (statement->get_parent() != target)
				// skip everything that is not
				// on the top level for now
				continue;
			dispatch_stmt(statement);
		}

		// Remove statements left after propagation
		if (p.remove_unused)
			remove_unused_assignments(removal_possible());
	} else if (auto f = isSgFunctionDefinition(target)) {
		target = f->get_body();
		traverse_block();
	} else {
		l << target->unparseToString() << endl;
		assert(!"Unsupported statement");
	}
	// rd.reset(); // really?
	if (log)
		l << LOG_INFO << "Propagation statistics: " << p_stats << endl;
}

// ------------> Inliner
// TODO push to inliner.cpp?

namespace {

/// Renames *iname* to *newn* by transmogrifying symbol tables.
void rename(SgInitializedName* iname, std::string newn)
{
	auto oldn = iname->get_name().getString();
	auto st = iname->get_scope()->get_symbol_table();
	auto old_entry = isSgVariableSymbol(st->find(iname));
	assert(old_entry);
	st->remove(old_entry);
	iname->set_name(newn);
	auto renamed = new SgVariableSymbol(iname);
	renamed->set_parent(st);
	st->insert(newn, renamed);
}

/// Replaces all references to *o* in indexes by *n*
void refresh_refs(SgArrayType* t, std::string& o, std::string& n)
{
	if (!t)
		return;
	auto dim = t->get_index();
	for (auto r : querySubTree<SgVarRefExp>(dim)) {
		if (get_var_name_from_node(r) == o)
			rename(get_var_initialized_name(r), n);
	}
	refresh_refs(isSgArrayType(t->get_base_type()), o, n);
}

/// Rename variables so that there will be no
/// collisions with variables declared in surrounding scopes
/// after block flattening.
std::size_t rnm_overlaps(SgBasicBlock* b)
{
	using namespace std::placeholders;
	using namespace boost::adaptors;
	auto r = static_cast<std::size_t>(0);
	for (auto d : querySubTree<SgVariableDeclaration>(b)) {
		auto iname = getFirstInitializedName(d);
		auto name = iname->get_name();
		auto enc_scope = getEnclosingNode<SgScopeStatement>(d);
		auto coll = lookupVariableSymbolInParentScopes(name, enc_scope);
		if (coll) {
			auto new_name =
				strip(generateUniqueVariableName(b, name));
			rename(iname, new_name);
			// Update references in array types. They are not
			// handled automatically as for some reason they have a
			// separate (possibly a set of) symbol table(s).
			// \todo check if there is only one symbol table for
			// vars in types and use it?
			auto t = iname->get_scope()->get_symbol_table();
			using boost::placeholders::_1;
			boost::for_each(
				*(t->get_table())
				| map_values
				| transformed([] (auto t) {
					return isSgArrayType(t->get_type());
				})
				| filtered([] (auto t) {
					return t != nullptr;
				}),
				boost::bind(refresh_refs, _1,
					name.getString(), new_name));
			r++;
		}
	}

	return r;
}

/// Transfers code like
///
///     int var;
///     var = 5;
///     x = f(var);
/// into
///
///     x = f(5);
/// Collects all variables that are defined right after declaration
/// and tries to propagate them.
void remove_temporary_vars(SgScopeStatement* s)
{
	STRS tmp_vars;
	//auto decls = querySubTree<SgVariableDeclaration>(s)
	//                | boost::adaptors::filtered([] (auto d) {
	//                        return !isSgForInitStatement(d->get_parent());
	//                });
	for (auto d : querySubTree<SgVariableDeclaration>(s)) {
		if (isSgForInitStatement(d->get_parent()))
			continue;
		if (!isSomeAssignment(getNextStatement(d)))
			continue;
		tmp_vars.insert(getFirstInitializedName(d)->get_name());
	}
	l << tmp_vars << endl;
	if (tmp_vars.empty())
		return;
	BlockTraveller::BTParams satchel;
	//satchel.local = tmp_vars;
	satchel.propagate = std::move(tmp_vars);
	BlockTraveller traveller(s, std::move(satchel), false);
	traveller.traverse_block();
}

STRV glibc_fns;
// This should be a link to ./libc-functions!
auto glibc_list_path = std::string(getpwuid(getuid())->pw_dir)
			+ "/.libc-functions";

bool fill_libc_fs()
{
	std::fstream f(glibc_list_path);
	if (!f.is_open()) {
		l << LOG_WARN << "Couldn't open file " << glibc_list_path;
		l << ", which is required to filter out glibc functions during inlining. ";
		l << endl;
		return false;
	}
	l << "Reading libc function list" << endl;
	while (!f.eof()) {
		std::string fn;
		f >> fn;
		glibc_fns.push_back(std::move(fn));
	}
	l << glibc_fns.size() << " functions read." << endl;
	return glibc_fns.size();
}

bool is_libc_f(std::string& fn)
{
	if (glibc_fns.empty() && !fill_libc_fs())
		return false;
	return boost::binary_search(glibc_fns, fn);
}

/// This does most of the main inlining work.
/// *Inlined* is passed in as a pointer because this class
/// is used as a functor in find_if() which seems to make a copy of it
/// and we are unable to get the contents of the classes' variables.
class InlineWrapper {
	std::size_t* inlined;
	bool everything;
	TfUtils::STRV names;
	std::deque<SgStatement*> statements;
	typedef std::function<SgNode*(SgNode*)> CopyImpl;
	CopyImpl copy_impl;
public:
	InlineWrapper(TfUtils::STRV&& names, SgStatement* stmt,
			CopyImpl copy_impl, std::size_t* inlined)
		: inlined(inlined), everything(names.empty()),
		names(std::move(names)), copy_impl(copy_impl)
	{
		statements.push_back(stmt);
	}

	/// A doInline() wrapper; checks inlining possibility and calls the
	/// main function, handles errors.
	/// If inlining succeeded, inserts the resulting block into *statements*.
	void operator()(SgFunctionCallExp* call)
	{
		auto fn = call->getAssociatedFunctionSymbol()
				->get_name().getString();
		if (!everything && (std::find(names.begin(), names.end(), fn)
								== names.end()))
			return; // not inlining this call
		if (everything && is_libc_f(fn)) {
			l << "Not inlining " << fn;
			l << " as it is a glibc function. To inline it, specify its name in the corresponding pragma.";
			l << endl;
			return; // not inlining this call
		}
		if (auto block = inline_function(call, true, copy_impl)) {
			(*inlined)++;
			auto renamed = rnm_overlaps(block);
			l << call << "Inlined a call to " << fn;
			l << " and renamed " << renamed;
			l << " variables." << endl;
			statements.push_back(block);
		} else {
			l << LOG_WARN << call;
			l << "Couldn't inline a call to " << fn << endl;
			return;
		}
	}

	/// For each statement in *statements*, this funtion queries all
	/// function calls in it and tries to inline them.
	/// All function calls are handled with operator() above.
	///
	/// Returns whether there are any statements left.
	bool operator()(std::size_t d)
	{
		for (auto i = statements.size(); i != 0; i--) {
			auto stmt = statements.front();
			statements.pop_front();
			auto calls = querySubTree<SgFunctionCallExp>(stmt);
			*this = boost::for_each(calls, *this);
		}
		return statements.empty();
	};
};
} // namespace

/// Inlines calls to functions from *names*
/// (or to all functions if *names* is empty)
/// iteratively with no more than *depth* iterations.
///
/// \todo What do they mean by defining where a transformation happened,
/// or why does cleanupInlinedCode(...) fails?
/// \todo What's markAsTransformation()?
void inline_calls(SgStatement *stmt, TfUtils::STRV&& names, std::size_t depth,
			TfUtils::CopyImpl copy_impl/* = TfUtils::CopyImpl()*/)
{
	using namespace boost::range_detail;
	assert(stmt);
	// \todo get enc scope, not function?
	//auto enc_f = getEnclosingFunctionDefinition(stmt);
	auto enc_f = getEnclosingNode<SgScopeStatement>(stmt);
	std::size_t inlined = 0;
	auto wr = InlineWrapper(std::move(names), stmt, copy_impl, &inlined);
	auto it = boost::find_if(boost::irange(std::size_t(0), depth), wr);
	auto iter = std::distance(boost::range_detail::integer_iterator<std::size_t>(0), it);
	if (iter) {
		l << LOG_INFO << "Inlined " << inlined << " function calls in ";
		l << iter + 1 << " iterations." << endl;
	}

	flattenBlocks(enc_f);

	SageInterface::removeJumpsToNextStatement(enc_f);
	SageInterface::removeUnusedLabels(enc_f);
	removeNullStatements(enc_f);
	//remove_temporary_vars(enc_f);
}

#include <stdio.h>

// Check some inter-procedural stuff.

#define m 4
#define n 4

void full_no_dep(double a[m][n])
{
	for (unsigned i = 0; i != m; i++ )
		for (unsigned j = 0; j != m; j++ )
			a[i][j] = a[i][j] + 3.1415;
}

void full_dep(double a[m][n])
{
	for (unsigned i = 1; i != m; i++ )
		for (unsigned j = 0; j != m; j++ )
			a[i][j] = a[i-1][j] + 3.1415;
}

int main()
{
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,6,7,8};
	int cc[] = {1,2,3,4,5,6,6,7,8};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	#pragma a dependency
	{
	for (unsigned iter = 0; iter < bk; iter++) {
		full_dep(a);
	}
	for (unsigned iter = 0; iter < bk; iter++) {
		full_no_dep(a);
	}
	}
	return 0;
}

//+CA
//+TL 19 19  i | > | 1 | |
//+FL 12 12 *

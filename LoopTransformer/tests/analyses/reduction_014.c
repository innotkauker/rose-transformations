#include <stdio.h>
#include <stdbool.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double x[4] = {1,2,3,4};
	double y[4] = {1,2,3,4};
	bool r0[4] = {false, false, false, false};
	bool r1[4] = {false, false, false, false};
	bool r2[4] = {false, false, false, false};
	bool r3[4] = {false, false, false, false};
	bool r4[4] = {false, false, false, false};
	bool r5[4] = {false, false, false, false};
	bool r6[4] = {false, false, false, false};
	bool r7[4] = {false, false, false, false};
	bool r8[4] = {false, false, false, false};
	bool r9[4] = {false, false, false, false};
	bool r10[4] = {false, false, false, false};
	#pragma a reduction
	{
	for (int i = 0; i < m; i++) {
		if (x[i] != y[i])
			r0[i] = false;
		else
			r0[i] = true;
	}
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r1[i] = true;
		else
			r1[i] = false;
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r2[i] = true;
	for (int i = 0; i < m; i++)
		r3[i] = x[i] == y[i];
	for (int i = 0; i < m; i++)
		r4[i] = (x[i] == y[i]);
	for (int i = 0; i < m; i++) {
		bool tmp = x[i] == y[i];
		r5[i] = tmp;
	}
	for (int i = 0; i < m; i++) {
		double xx = x[i];
		double yy = y[i];
		r6[i] = (xx == yy);
	}
	for (int i = 0; i < m; i++) {
		double yy = y[i];
		r7[i] = (x[i] == yy);
	}
	for (int i = 0; i < m; i++)
		r8[i] = x[i] != y[i];
	for (int i = 0; i < m; i++) {
		double yy = y[i];
		r9[i] = (yy != x[i]);
	}
	for (int i = 0; i < m; i++) {
		bool tmp = x[i] == y[i];
		r10[i] = !tmp;
	}
	}
	return 0;
}
//+T pragma PRG REDUCTION(NEQV(r0))
//+T pragma PRG REDUCTION(EQV(r1))
//+T pragma PRG REDUCTION(EQV(r2))
//+T pragma PRG REDUCTION(EQV(r3))
//+T pragma PRG REDUCTION(EQV(r4))
//+T pragma PRG REDUCTION(EQV(r5))
//+T pragma PRG REDUCTION(EQV(r6))
//+T pragma PRG REDUCTION(EQV(r7))
//+T pragma PRG REDUCTION(NEQV(r8))
//+T pragma PRG REDUCTION(NEQV(r9))
//+T pragma PRG REDUCTION(NEQV(r10))

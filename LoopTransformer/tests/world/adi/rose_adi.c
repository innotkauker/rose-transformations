/* ADI program */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define nx 384
#define ny 384
#define nz 384
void init(double (*a)[384][384]);

int main(int argc,char *argv[])
{
        double maxeps;
        double eps;
        double (*a)[384][384];
        int it;
        int itmax;
        int i;
        int j;
        int k;
        double startt;
        double endt;
        maxeps = 0.01;
        itmax = 50;
        a = ((double (*)[384][384])(malloc(56623104 * sizeof(double ))));
        init(a);
#ifdef _DVMH
#else
        startt = 0;
#endif
        for (it = 1; it <= itmax; it++) {
                eps = 0;
{
                        for (i = 1; i < 383; i++) {
#pragma omp parallel for
                                for (j = 1; j < 383; j++) {
                                        for (k = 1; k < 383; k++) {
                                                a[i][j][k] = (a[i - 1][j][k] + a[i + 1][j][k]) / 2;
                                        }
                                }
                        }

#pragma omp parallel for
                        for (i = 1; i < 383; i++) {
                                for (j = 1; j < 383; j++) {
                                        for (k = 1; k < 383; k++) {
                                                a[i][j][k] = (a[i][j - 1][k] + a[i][j + 1][k]) / 2;
                                        }
                                }
                        }

#pragma omp parallel for reduction(max:eps)
                        for (i = 1; i < 383; i++) {
                                for (j = 1; j < 383; j++) {
                                        for (k = 1; k < 383; k++) {
                                                double tmp1 = (a[i][j][k - 1] + a[i][j][k + 1]) / 2;
                                                double tmp2 = fabs(a[i][j][k] - tmp1);
                                                eps = (eps > tmp2?eps : tmp2);
                                                a[i][j][k] = tmp1;
                                        }
                                }
                        }
                }
                printf(" IT = %4i   EPS = %14.7E\n",it,eps);
                if (eps < maxeps)
                        break;
        }
#ifdef _DVMH
#else
        endt = 0;
#endif
        free(a);
        printf(" ADI Benchmark Completed.\n");
        printf(" Size            = %4d x %4d x %4d\n",384,384,384);
        printf(" Iterations      =       %12d\n",itmax);
        printf(" Time in seconds =       %12.2lf\n",endt - startt);
        printf(" Operation type  =   double precision\n");
        printf(" Verification    =       %12s\n",(fabs(eps - 0.07249074) < 1e-6?"SUCCESSFUL" : "UNSUCCESSFUL"));
        printf(" END OF ADI Benchmark\n");
        return 0;
}

void init(double (*a)[384][384])
{
        int i;
        int j;
        int k;
{

#pragma omp parallel for
                for (i = 0; i < 384; i++) {
                        for (j = 0; j < 384; j++) {
                                for (k = 0; k < 384; k++) {
                                        if (k == 0 || k == 383 || j == 0 || j == 383 || i == 0 || i == 383)
                                                a[i][j][k] = 10.0 * i / 383 + 10.0 * j / 383 + 10.0 * k / 383;
                                         else
                                                a[i][j][k] = 0;
                                }
                        }
                }
        }
}

#include <stdio.h>

int f(int x)
{
  int r = x;
  for (int i = 0; i < x; i++) 
    r *= r;
  return r;
}

double g(double x,double y)
{
  return x * x - y * y;
}

int main()
{
  int a = 5;
  double b = 0.42;
  double c = 5e2;
  
#pragma x inline(f, g)
{
    int rose_temp__4;
{
      int _x_20 = a;
      int _r1 = _x_20;
      for (int _i2 = 0; _i2 < _x_20; _i2++) 
        _r1 *= _r1;
{
        rose_temp__4 = _r1;
        goto rose_inline_end__3;
      }
      rose_inline_end__3:
      ;
    }
    double rose_temp__9;
{
      double _x_63 = b;
      double _y_74 = c;
{
        rose_temp__9 = _x_63 * _x_63 - _y_74 * _y_74;
        goto rose_inline_end__8;
      }
      rose_inline_end__8:
      ;
    }
    printf("%i, %f, ",rose_temp__4,rose_temp__9);
    int rose_temp__13;
{
      int _x_115 = b;
      int _r6 = _x_115;
      for (int _i7 = 0; _i7 < _x_115; _i7++) 
        _r6 *= _r6;
{
        rose_temp__13 = _r6;
        goto rose_inline_end__12;
      }
      rose_inline_end__12:
      ;
    }
    double rose_temp__18;
{
      double _x_158 = c;
      double _y_169 = a;
{
        rose_temp__18 = _x_158 * _x_158 - _y_169 * _y_169;
        goto rose_inline_end__17;
      }
      rose_inline_end__17:
      ;
    }
    printf("%i, %f, ",rose_temp__13,rose_temp__18);
  }
}

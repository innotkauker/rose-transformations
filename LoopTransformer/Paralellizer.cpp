#include "Paralellizer.h"
#include "sage3basic.h"
#include "util.h"
#include "ReductionAnalysis.h"
#include "DependencyAnalysis.h"

using namespace SageInterface;
using namespace TfUtils;

Paralellizer::Paralellizer(SgNode* root)
	: root(root)
{
}

Paralellizer::~Paralellizer() = default;

// TODO loop needs to be canonical by omp standards
void Paralellizer::paralellize(SgForStatement* loop, const std::vector<CodeAnalyses::Reduction>& reductions)
{
	using namespace SageBuilder;
	std::stringstream ps;
	ps << "omp parallel for";
	if (reductions.size() == 1)
		ps << " " << reductions[0].to_omp_string();
	auto pragma = buildPragmaDeclaration(ps.str(), getEnclosingScope(loop));
	insertStatementBefore(loop, pragma);
	l << LOG_INFO << "Inserting omp parallel for pragma for loop at " << loop << std::endl;
}

// TODO things to consider.
// * Inlining - all-into-main? Should not be needed for omp, but cuda is a different thing.
// * omp may also need something like this perhaps...
void Paralellizer::run()
{
	using namespace CodeAnalyses;
	auto nests = query_loop_nests(root);
	// We only try to paralellize topmost loops in each function.
	// TODO what if only the inner nest can be paralellized? Need to check all and then choose...
	// nests.erase(boost::remove_if(nests, [] (auto nest) {
				// return getEnclosingNode<SgForStatement>(nest);
			// }), nests.end());
	for (auto nest : nests) {
		std::deque<SgInitializedName*> nest_indices; // indices of all loops in this nest and those surrounding them, from outermost to innermost.
		// this may be needed actually...
		// auto loop = nest;
		// while (loop = getEnclosingNode<SgForStatement>(loop)) {
			// NV loop_indices;
			// get_modified_variables(get_exp_with_loop_indices(loop), loop_indices);
			// nest_indices.push_front(loop_indices.begin(), loop_indices.end());
		// }
		get_nested_loops_indices(nest, nest_indices);
		ReductionAnalysis ra(nest);
		const auto& reductions = ra.run(false);
		NS reduction_vars;
		using namespace boost::adaptors;
		boost::copy(reductions | transformed([] (const Reduction& r) {
						return r.name;
					}), std::inserter(reduction_vars, reduction_vars.end()));
		Dependency loop_dependency;
		auto dirv_attr = dynamic_cast<DirvAttribute*>(nest->getAttribute(DIRV_ATTRIBUTE));
		if (dirv_attr) {
			auto dirv = dirv_attr->get_directions();
			// TODO this isn't an assert -- user could have supplied a wrong argument number.
			// But need it for testing.
			assert(nest_indices.size() == dirv.size());
			loop_dependency = Dependency(nest, nest_indices);
			for (unsigned i = 0; i != nest_indices.size(); i++) {
				loop_dependency.loop().set_direction(i, dirv[i]);
			}
			l << "Using user-provided dependency:" << std::endl;
			loop_dependency.dump();
		} else {
			DependencyAnalysis a(nest);
			a.run();
			auto dependencies = a.take_result();
			for (auto&& d : dependencies) {
				d.dump();
				if (!d.is_loop_carried() && std::find(reduction_vars.begin(),
								reduction_vars.end(),
								d.get_var()) != reduction_vars.end())
					d.make_independent(); // This dependency is a reduction
				loop_dependency = std::move(loop_dependency).splice(std::move(d));
				if (loop_dependency.hopeless())
					break;
			}
			if (loop_dependency.exists() && !loop_dependency.is_loop_carried())
				continue;
		}
		// first case - uppermost loop doesn't have dependencies
		bool topmost_free = loop_dependency.is_loop_carried()
			&& loop_dependency.loop().get_direction(nest_indices[0]) == Direction::none;
		if (!loop_dependency.exists() || topmost_free) {
			paralellize(nest, reductions);
		}
	}
}


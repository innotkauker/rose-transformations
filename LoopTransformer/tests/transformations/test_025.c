#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t, t1, t2;
	#pragma x propagate(t, vol)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			double vol = 0.1;
			t = 15;
			t1 = i;
			while (t1-- > 0) {
				if ((vol += 0.3) > 2)
					vol += 0.2;
				else
					vol += 0.4;
				#pragma x unroll
				for (int ii = 0; ii < 3; ii++) {
					int tmp = ii + i;
					vol += 0.1*tmp;
				}

			}
			a[i][j] = (double)t/vol;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}



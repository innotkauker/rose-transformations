#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        int a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        int t = 0;
        int t1 = 3;
        int t2 = 5;
        int c[2] = {(2), (3)};
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        for (unsigned int k = 0; k < 2; k++) {
                                c[k] = (k * k);
                        }
                        a[i][j] = ((i?c[0] : c[1])) + (((int )j) + 1) / 2;
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}

#include <stdio.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	const unsigned k = 3;
	int a[m][n];
	double bc[k][k];
	double bs[k][k];
	double c[k];
	int j, i;
	double t = 0, t1 = 3, t2 = 5;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;
	for (unsigned i = 0; i < k; i++) {
		c[i] = i*k*6 - 3*i - 2;
		for (unsigned j = 0; j < k; j++) {
			bs[i][j] = i*i*3 + i;
			bc[i][j] = j*k*6 - 5*j + i + 8;
		}
	}

	// here c[u] should'n be propagated as that's another u
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t, c2, c, vol)
		for (unsigned j = 0; j < 4; j++) {
			vol = 50;
			for (int u = 0; u < k; u++) {
				c[u] = vol*u;
				vol += c[u];
			}
			vol = j;
			for (int u = 0; u < k; u++) {
				vol += c[u];
				a[i][j] -= vol*k;
			}
			a[i][j] = c[1] - vol + t;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

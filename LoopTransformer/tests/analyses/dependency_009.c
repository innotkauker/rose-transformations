#include <stdio.h>
#include <math.h>
#define Max(a, b) ((a) > (b) ? (a) : (b))

// Regular variables
int main()
{
	unsigned p, q, l;
	double a, b, d;
	const unsigned m = 4;
	const unsigned n = 4;

	int j;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, total = 0, t = 0;
	const int N = 1;
	const int M = 2;
	double x[] = {1,2,3,4,5,6,6,7,8};
	double aa[m][n][m][n];
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double eps = 0;
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			for (unsigned k = 0; k < m; k++)
				for (unsigned l = 0; l < n; l++)
					aa[i][j][k][l] = i*j*3 - i + k + l*j*2 + l - 2;
	#pragma a dependency
	{
	double r1, r2, r3, r4, r5, t1, t2;
	for (int k = 1; k <= m; k++)
	{
		for (int j = 1; j <= n; j++)
		{
			for (int i = 1; i <= m; i++)
			{
				r5 = r5 + aa[k][j][i][4];

				t2 = 0.5 * (r5);
				aa[k][j][i][4] = 5.4 * t2;
				r5 = aa[k][j][i][3];

				t2 = 0.5 * (r5);
				aa[k][j][i][3] = 5.4 * t2;
			}
		}
	}
	for (int i = 0; i < m; i++) {
		c[i] = fabs(c[i] - ct[i]);
	}
	for (int i = 0; i < m; i++) {
		double tmp = fabs(c[i] - ct[i]);
		eps = Max(tmp, eps);
		c[i] = ct[i];
	}
	}
	return 0;
}

//+CA
//+FL 30 *
//+FL 39 *
//+FL 40 *
//+FL 41 *
//+FL 42 *
//+FL 66 66 *
//+FL 76 80 *
//+FL 78 82 *
//+FL 78 80 *
//+FL 79 82 *
//+FL 49 49 *


#pragma once
#include "sage3basic.h"
#include "util.h"

extern const std::string PROFILE_ATTRIBUTE;
class ProfileAttribute: public AstAttribute {
	double runtime; // seconds
public:
	ProfileAttribute(double t): runtime(t) {}
	unsigned get_runtime() { return runtime; }
	virtual std::string attribute_class_name() const override
	{
		return std::string("ProfileAttribute");
	}

	virtual AstAttribute* copy() const override
	{
		return new ProfileAttribute(runtime);
	}
};


// Checks the execution times of (given?) loop nests, attaches that information to AST or prints it to console.
class Profiler {
	TfUtils::SV targets;

	struct Instrumentation {
		SgStatement* stmt{nullptr};
		SgStatement* prestart{nullptr};
		SgStatement* postend{nullptr};
		double time{0};
	};
	std::vector<Instrumentation> instruments;

	Instrumentation create_inst(SgStatement *stmt, unsigned index);

public:
	Profiler(SgProject *p);
	void profile();
	void dump();
	void annotate();
};

std::string exec(std::string cmd);

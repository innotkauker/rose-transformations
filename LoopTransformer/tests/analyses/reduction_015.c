#include <stdio.h>
#include <stdbool.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double x[4] = {1,2,3,4};
	double y[4] = {1,2,3,4};
	bool r0[4] = {false, false, false, false};
	bool r01[4] = {false, false, false, false};
	bool r02[4] = {false, false, false, false};
	bool r1[4] = {false, false, false, false};
	bool r2[4];
	bool r3[4] = {false, false, false, false};
	bool r4[4] = {false, false, false, false};
	bool r41[4] = {false, false, false, false};
	bool r5[4] = {false, false, false, false};
	bool r61[4] = {false, false, false, false};
	#pragma a reduction
	{
	for (int i = 0; i < m; i++) {
		if (x[i+1] != y[i])
			r0[i] = false;
		else
			r0[i] = true;
	}
	for (int i = 0; i < m; i++) {
		if (x[i] != y[i])
			r01[i+1] = false;
		else
			r01[i] = true;
	}
	for (int i = 0; i < m; i++) {
		if (x[i] != y[i])
			r02[i] = false;
		else
			r02[i] = false;
	}
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r1[i] = true;
		else
			r1[i] = true;
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r2[i] = true;
	for (int i = 0; i < m; i++)
		r3[i] = x[i+2] == y[i];
	for (int i = 0; i < m; i++)
		r4[i] = (x[i] == y[1+i]);
	for (int i = 0; i < m; i++)
		r41[i] = (x[i] == 2 + y[i]);
	for (int i = 0; i < m; i++) {
		bool tmp = x[i] == y[i];
		r5[i] = !tmp;
	}
	for (int i = 0; i < m; i++) {
		double xx = x[1+i];
		double yy = y[1+i];
		r61[i] = (xx == yy);
	}
	}
	return 0;
}
//+F pragma PRG REDUCTION(NEQV(r0))
//+F pragma PRG REDUCTION(EQV(r0))
//+F pragma PRG REDUCTION(NEQV(r01))
//+F pragma PRG REDUCTION(EQV(r01))
//+F pragma PRG REDUCTION(NEQV(r02))
//+F pragma PRG REDUCTION(EQV(r02))
//+F pragma PRG REDUCTION(NEQV(r1))
//+F pragma PRG REDUCTION(EQV(r1))
//+F pragma PRG REDUCTION(NEQV(r2))
//+F pragma PRG REDUCTION(EQV(r2))
//+F pragma PRG REDUCTION(EQV(r3))
//+F pragma PRG REDUCTION(EQV(r4))
//+F pragma PRG REDUCTION(EQV(r41))
//+F pragma PRG REDUCTION(EQV(r5))
//+F pragma PRG REDUCTION(EQV(r61))

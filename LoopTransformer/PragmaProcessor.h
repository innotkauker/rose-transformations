#pragma once

#include "sage3basic.h"
#include <string>
#include <set>
#include <vector>
#include <map>
#include "util.h"

/// This class handles user input - parses commandline parameters,
/// pragmas, and calls transforming or analysing code according to
/// the extracted orders.
class PragmaProcessor {
	SgProject* project;
	bool inline_all;
	bool profile;
	bool paralellize;
	bool build_interception;
	bool make_collector;
	bool compile;
	int indentation;
public:
	PragmaProcessor(int argc, char *argv[]);
	bool needs_processing() { return !make_collector && !build_interception; }
	/// Parses pragmas and performs the requests.
	void process();
	/// Unparses the result or calls backend depending on *compile* value.
	/// \todo add filename as a parameter?
	int backend();
};


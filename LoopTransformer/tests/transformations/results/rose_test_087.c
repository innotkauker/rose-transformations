#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        double a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        double bc[3][3] = {{(0), (1)}, {(1), (2)}};
        double bs[3][3] = {{(0), (1)}, {(1), (2)}};
        int t = 0;
        int t1 = 3;
        int t2 = 5;
        int k = 0;
        double vol = 0;
        double vol0 = 0;
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        bc[2][2] = (t1 + t2 * i - i * 5 * j) + 2 * (a[0][1] - a[i][j]);
                        vol = (bc[2][2] - ((double )(((unsigned int )t1) + ((unsigned int )t2) * i - i * ((unsigned int )5) * j))) / ((double )2) * ((bc[1][1] - ((double )3)) / ((double )2)) - (((double )(((unsigned int )3) / (j + ((unsigned int )3)))) - bc[2][1]) / ((double )2) * ((bc[1][2] - ((double )(i * j))) / ((double )-2));
                        bc[1][1] = bc[1][1] / vol;
                        bc[1][2] = bc[1][2] / vol;
                        bc[2][1] = bc[2][1] / vol;
                        bc[2][2] = bc[2][2] / vol;
                        a[i][j] += vol + bc[1][1] * bc[2][2] - bc[1][2] * bc[2][1];
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%f \n",a[i][j]);
}

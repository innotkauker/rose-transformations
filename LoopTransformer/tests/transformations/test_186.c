#include <stdio.h>

int main()
{
	const unsigned m = 10;
	const unsigned n = 10;
	double a[m][n];
	double b[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3.8347568324 - i*1.456234657 + j*2.0456245 + 23;


	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			b[i][j] = j + 0.4363534e5*i;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			printf("%f ", a[i][j]);
			printf("%f \n", b[i][j]);
		}

}

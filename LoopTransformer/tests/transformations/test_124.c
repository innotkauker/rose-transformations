#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5, k = 0, vol = 0;
	int c[2] = {2, 3};
	int c2[2][2] = {{2, 3}, {4, 5}};
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t, c2, c, vol)
		for (unsigned j = 0; j < 4; j++) {
			vol = 50;
			vol = j;
			int xx = 0, yy = 0;
			#pragma x propagate(xx, yy)
			for (int k = 0; k < 2; k++) {
				c2[xx][yy] = k*i*j;
				c[k] = vol*k;
				vol += c[k];
			}
			a[i][j] = c[1] - vol + t + c2[0][0];
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

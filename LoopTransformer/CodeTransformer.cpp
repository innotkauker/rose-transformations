#include "CodeTransformer.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include "sage3basic.h"
#include "UnparseFormat.h"
#include "LoopUnroller.h"
#include "util.h"
#include "MintConstantFolding.h"
#include "LoopTransformations.h"
#include "StatementTransformations.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace AstFromString;
using namespace LoopTransformations;
using namespace TfUtils;

namespace {
using Test = std::function<bool(void)>;
using TestV = std::vector<Test>;
using Message = const char*;
using MessageV = std::vector<Message>;

void throw_if_true(Test&& test, Message m, SgNode *where)
{
	if (test())
		throw Exception(m, where);
}

void perform_tests(TestV&& tests, MessageV&& messages, SgNode *where)
{
	assert(tests.size() == messages.size());
	for (size_t i = 0; i < tests.size(); i++)
		throw_if_true(std::move(tests[i]), messages[i], where);
}
} // namespace

void CodeTransformer::transformation_compatibility_check()
{
	decltype(auto) p = this->p;
	TestV tests = {
		[&p] {
			return how_many_of(p.distribute, p.unroll,
					!p.unroll_vars.empty(),
					p.partition, !p.split_vars.empty()) > 1;
		},
		[&p] {
			bool loop_tr = p.unroll || !p.unroll_vars.empty()
					|| p.partition || !p.split_vars.empty()
					|| p.distribute;
			bool body_tr = !p.swap_indices.empty()
					|| !p.merge_indices.empty();
			return loop_tr && body_tr;
		},
	};
	MessageV messages = {
		"You have requested more than one of unrolling, splitting, partitioning or partitioning for a single block",
		"Unable to modify the contents of the body of a loop while changing the loop itself",
	};
	perform_tests(std::move(tests), std::move(messages), block);
}

void CodeTransformer::construct_for_loop(SgForStatement *for_loop)
{
	assert(for_loop);
	get_nested_loops_indices(for_loop, loop_indices);
	if (!p.permutation_indices.empty() &&
		(loop_indices.size() != p.permutation_indices.size()))
		throw Exception("the number of variables in permutation list does not match the number of loop indices", block);
	body_block = get_nested_loops_main_block(block);
	assert(body_block);
}

void CodeTransformer::construct_for_block(SgBasicBlock *block)
{
	assert(block);
	decltype(auto) p = this->p;
	TestV tests = {
		[&p] {
			return !p.split_vars.empty() || p.partition
				|| p.distribute;
		},
		[&p] { return !p.permutation_indices.empty(); },
		[&p] { return !p.index_shift_vars.empty(); },
		[&p] { return p.unroll || !p.unroll_vars.empty(); },
	};
	MessageV messages = {
		"Unable to split or distribute a basic block.",
		"Cannot permutate indices of a basic block.",
		"Unable to shift indices of a basic block.",
		"Unable to unroll a basic block.",
	};
	perform_tests(std::move(tests), std::move(messages), block);
	body_block = block;
}

void CodeTransformer::construct_for_stmt(SgStatement *stmt)
{
	assert(stmt);
	decltype(auto) p = this->p;
	TestV tests = {
		[&p] { return !p.propagate_locally.empty(); },
		[&p] { return !p.bringout_vars.empty(); },
		[&p] { return p.partition || p.distribute || !p.split_vars.empty(); },
		[&p] { return !p.permutation_indices.empty(); },
		[&p] { return !p.mapping_vars.empty(); },
		[&p] { return !p.index_shift_vars.empty(); },
		[&p] { return p.unroll || !p.unroll_vars.empty(); },
		[&p] { return p.remove; },
		[&p] { return p.bringout_everything; },
		[&p] { return !p.swap_indices.empty(); },
		[&p] { return !p.merge_indices.empty(); },
	};
	MessageV messages = {
		"Unable to propagate locally into a statement.",
		"Unable to bringout a statement from itself.",
		"Can't split a statement.",
		"Index permutation is available only for loops.",
		"Variable mapping is supported only for scoping statements.",
		"Index shifting is supported only for loops.",
		"No unrolling for non-loops!",
		"Removal is supported only for code blocks. This message is probably a bug.",
		"Can't turn on statement bringout for a non-scope statement!",
		"Code block swapping is supported only for scoping statements.",
		"Loop merging is supported only for scoping statements.",
	};
	perform_tests(std::move(tests), std::move(messages), stmt);
	body_block = stmt;
}

void CodeTransformer::construct_for_scope(SgScopeStatement *stmt)
{
	assert(stmt);
	decltype(auto) p = this->p;
	TestV tests = {
		[&p] { return !p.bringout_vars.empty(); },
		[&p] { return p.partition || p.distribute || !p.split_vars.empty(); },
		[&p] { return !p.permutation_indices.empty(); },
		[&p] { return !p.index_shift_vars.empty(); },
		[&p] { return p.unroll || !p.unroll_vars.empty(); },
		[&p] { return p.remove; },
		[&p] { return p.bringout_everything; },
	};
	MessageV messages = {
		"Statement bringout is supported only for (for) loops.",
		"Splitting is only for for-loops currently.",
		"Index permutation is available only for for-loops.",
		"Index shifting is supported only for loops.",
		"Unrolling is currenly supported for for-loops only.",
		"Removal is supported only for code blocks (just basic blocks). This message is probably a bug.",
		"Can't turn on statement bringout for a non-loop statement!",
	};
	perform_tests(std::move(tests), std::move(messages), stmt);
	body_block = stmt;
}

CodeTransformer::CodeTransformer(SgStatement *the_block, TransformParams&& p)
	: p(std::move(p)), block(the_block)
{
	transformation_compatibility_check();
	if (p.border)
		block->setAttribute(BORDER_ATTRIBUTE, nullptr);
	if (p.id != -1)
		block->setAttribute(ID_ATTRIBUTE, new IdAttribute(p.id));
	MintConstantFolding::constantFoldingOptimization(block);

	if (auto for_loop = isSgForStatement(block)) {
		construct_for_loop(for_loop);
	} else if (auto basic_block = isSgBasicBlock(block)) {
		construct_for_block(basic_block);
	} else if (auto scope = isSgScopeStatement(block)) {
		construct_for_scope(scope);
	} else if (auto stmt = isSgStatement(block)) { // \todo really any statement?
		construct_for_stmt(stmt);
	} else { // this code is dead.
		throw Exception("Unsupported construction", block);
	}
}

/// Replaces *block* with its contents
///
void CodeTransformer::remove_temporary_block(SgBasicBlock *block)
{
	assert(block);
	insertStatementListBefore(block, block->get_statements());
	removeStatement(block);
	// deepDelete(block); // leaks only the block itself
}

/// Performs operations provided during class construction
/// on the supplied block.
///
void CodeTransformer::handle_block()
{
	// Inline calls
	if (p.inline_everything || !p.functions_to_inline.empty())
		inline_calls(body_block, std::move(p.functions_to_inline), p.idepth);

	// Perform propagation and mapping
	if (!p.propagate_vars.empty() || !p.mapping_vars.empty()) {
		BlockTraveller::BTParams satchel;
		satchel.propagate = p.propagate_vars;
		satchel.local = p.propagate_locally;
		satchel.mapping = p.mapping_vars;
		BlockTraveller traveller(body_block, std::move(satchel));
		traveller.traverse_block();
	}

	// Handle invariant bringout
	if (p.bringout_everything || !p.bringout_vars.empty())
		bringout_statements(isSgForStatement(block),
				p.bringout_everything,
				std::move(p.bringout_vars));

	// Permutate nested loops
	if (!p.permutation_indices.empty())
		permutate_loops(isSgForStatement(block),
				std::move(p.permutation_indices));
	// Shift indices of some
	if (!p.index_shift_vars.empty())
		shift_indices(isSgForStatement(block),
				std::move(p.index_shift_vars));
	// Finally unroll
	if (p.unroll || !p.unroll_vars.empty())
		unroll_some_iterations(isSgForStatement(block),
				std::move(p.unroll_vars));
	// Or split
	if (!p.split_vars.empty())
		split_loop(isSgForStatement(block), std::move(p.split_vars));
	if (p.distribute)
		distribute_loop(isSgForStatement(block));
	// Or partition it
	if (p.partition)
		partition_loop(isSgForStatement(block));

	// Swap some blocks
	if (!p.swap_indices.empty())
		swap_blocks(isSgBasicBlock(body_block),
				std::move(p.swap_indices));
	// Then merge some
	if (!p.merge_indices.empty())
		merge_loops(isSgBasicBlock(body_block),
			std::move(p.merge_indices));
	// And maybe remove unused block
	if (isSgBasicBlock(block) && p.remove)
		remove_temporary_block(isSgBasicBlock(block));
}


#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, y1 = 0;
	#pragma x distribute
	for (int i = 0; i < 4; i++) {
		t = i;
		y = y + 6;
		a[i][0] = 2*t*t;
		b[i][0] = 3*y*y;
		t1 = i;
		y1 = y1 + 6;
		a[i][0] += 2*t1*t1;
		b[i][0] -= 3*y*y1;
	}
	#pragma x distribute
	for (int i = 0; i < 4; i++) {
		t = i;
		for (int j = 0; j < i; j++)
			t++;
		y = (i ? t : -t);
		if (y == t)
			t = t*t + t1;
		while (t--)
			a[i][1]--;
		for (int j = 0; j < 4; j++)
			b[i][j] = t + y;
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}

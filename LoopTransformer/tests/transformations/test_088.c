#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double bc[3][3], bs[3][3];
	int t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0, vol0 = 0;
	#pragma x propagate(vol) map(t0:t, t1:t0, t2:t1)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			float t = i*j;
			double t0 = 2 + i*t;
			int t1 = t0 - 5;
			t2 = t1/(i+1)/(j+1);

			vol = t + t0 + t1 +t2;

			a[i][j] += vol;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%f \n", a[i][j]);

}


#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	double y = 3, yt = 6;
	double maxc0 = c[0];
	double maxc1 = c[0];
	double maxc2 = c[0];
	double maxc3 = c[0];
	int loc[5] = { -1, -1, -1, -1, -1 };
	int loc0 = -1;
	int loc1 = -1;
	int loc2 = -1;
	#pragma a reduction
	{
	for (int i = 0; i < m; ++i) {
		y = c[i] + 3;
		if (maxc2 < y) {
			maxc2 = y;
			loc0 = i;
		}
	}
	for (int i = 0; i < m; ++i) {
		double y = c[i] + 3;
		if (maxc0 < y) {
			maxc0 = y;
			loc[0] = i;
		}
	}
	for (int i = 0; i < m; ++i) {
		double y = c[i];
		if (maxc1 < y) {
			loc1 = i*i;
			maxc1 = c[i];
		}
	}
	for (int i = 0; i < m; ++i) {
		if (maxc3 < c[i]) {
			maxc3 = c[i];
			loc2 = i*i + maxc1;
		}
	}
	}
	printf("%f %f ", maxc0, loc[0]);
	printf("%f %f ", maxc1, loc1);
	printf("%f %f ", maxc2, loc0);
	printf("%f %f ", maxc3, loc2);
	return 0;
}
//+T pragma PRG REDUCTION(MAXLOC(maxc0, loc, 5))
//+T pragma PRG REDUCTION(MAXLOC(maxc1, loc1, 1))
//+T pragma PRG REDUCTION(MAXLOC(maxc2, loc0, 1))
//+T pragma PRG REDUCTION(MAXLOC(maxc3, loc2, 1))

#include <stdio.h>
const unsigned n = 16;
const unsigned m = 4;

int f()
{
	int a[m][n];
	for (unsigned i = 0; i < 2; i++)
		for (unsigned j = 0; j < 2; j++)
			a[i][j] = i*j;
	#pragma x id(4)
	for (unsigned i = 0; i < 2; i++)
		for (unsigned j = 0; j < 2; j++)
			a[i][j] += i*j;
	for (unsigned i = 0; i < 2; i++)
		for (unsigned j = 0; j < 2; j++)
			printf("%d ", a[i][j]);
	return 0;
}

int main()
{
	int a[m][n];
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j, i;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;

	#pragma x id(0)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int c[2] = {0,1};
			a[i][j] = j*3;
		}

	#pragma x id(1)
	for (int i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = i*j*3;
			a[i][j] -= tmp;
		}

	#pragma x id(2)
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			a[i][j] += j*i*2;
		}

	#pragma x id(3)
	for (int i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = i*j*3;
			a[i][j] -= tmp;
		}

	#pragma x id(4)
	for (unsigned i = 0; i < m; i++)
		a[i][0] += i*2;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%d \n", a[i][j]);

}


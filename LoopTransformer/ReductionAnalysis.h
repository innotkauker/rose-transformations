#pragma once
#include "sage3basic.h"
#include "util.h"

namespace CodeAnalyses {
// \todo create a single wrapper function for this?

struct Reduction {
	SgInitializedName* name{nullptr};
	VariantT type{V_SgTypeUnknown};
	SgInitializedName* array{nullptr};
	std::size_t array_length{0};

	bool valid() const ;
	std::string to_string() const ;
	std::string to_omp_string() const ;
};

/// Detects reduction.
class ReductionAnalysis {
	SgForStatement* loop;
	TfUtils::DefinitionProvider rd;

	SgExpression* get_rd(SgExpression* e);

	TfUtils::SV detect_reduction_possibilities(SgForStatement* loop);

	bool determine_xrd_vars(SgExpression* e, bool& max,
			SgInitializedName*& acc, SgExpression*& arr);
	Reduction detect_xloc(SgStatement* cond, SgStatementPtrList& statements);
	Reduction detect_cond_min_max(SgInitializedName* acc,
					SgConditionalExp* cond);
	Reduction detect_min_max(SgStatement* cond, SgStatement* s);
	Reduction detect_eqv_reduction(SgPntrArrRefExp* assignee,
					SgExpression* assigner);
	Reduction detect_assignment_reduction(SgAssignOp* e);
	Reduction detect_extreme_reduction(SgIfStmt* ifs);

	Reduction detect_reduction(SgStatement* s);
public:
	ReductionAnalysis(SgForStatement* loop);
	~ReductionAnalysis() = default;
	ReductionAnalysis(const ReductionAnalysis&) = delete;
	ReductionAnalysis& operator=(const ReductionAnalysis&) = delete;
	std::vector<Reduction> run(bool annotate);

};
} // namespace CodeAnalyses

#include <stdio.h>

int f(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	return r;
}

void g(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	printf("%i\n", r);
}

int main()
{
	int a = 5;
	int r = 6;
	double b = 0.42;
	double c = 5e2;
	int results[6];
	#pragma x inline
	for (int i = 0; i < 6; i++) {
		results[i] = f(i);
		r++;
	}
	#pragma x inline
	for (int i = 0; i < 6; i++)
		g(i);
	for (int i = 0; i < 6; i++)
		printf("%i ", results[i]);
	printf("%i", r);
}


#include <stdio.h>
#include <assert.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	int a[m][n];
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j, i;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;

	#pragma x merge(0, 1, 2)
	{
	#pragma x id(0)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			a[i][j] = j*3;
		}

	#pragma x id(1)
	for (i = 0; i < m; i++) {
		for (unsigned j = 0; j < n; j++) {
			for (unsigned k = 0; k < 4; k++) {
				int tmp = i*j*k;
				a[i][j] -= tmp;
			}
			t1 = j;
		}
		t += 1;
	}

	#pragma x id(2)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = t*t1*2;
			a[i][j] += tmp;
		}
	}

	#pragma x id(3)
	for (unsigned j = 0; j < n; j++) {
		int tmp = t*t1*2;
		a[0][j] = a[0][j] + tmp;
	}

	#pragma x id(4)
	for (unsigned j = 0; j < n; j++) {
		int tmp = t*t1*2;
		for (int i = 0; i < m; i++)
			a[i][j] = a[i][j] + tmp;
	}

	#pragma x id(5)
	for (int k = 0; k < m; k++)
	for (i = 0; i < m; i++)
	for (unsigned j = 0; j < n; j++) {
		int tmp = t*t1*2;
		a[0][j] = a[0][j] + tmp;
	}

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%d \n", a[i][j]);

}


#include <stdio.h>

int f(int x)
{
        int r = x;
        for (int i = 0; i < x; i++) 
                r *= r;
        return r;
}

void g(int x)
{
        int r = x;
        for (int i = 0; i < x; i++) 
                r *= r;
        printf("%i\n",r);
}

int main()
{
        int a = 5;
        int r = 6;
        double b = 0.42;
        double c = 5e2;
        int results[6];
        for (int i = 0; i < 6; i++) {
                int _x_53 = i;
                int _r4 = _x_53;
                for (int _i5 = 0; _i5 < _x_53; _i5++) 
                        _r4 *= _r4;
                results[i] = _r4;
                r++;
        }
        for (int i = 0; i < 6; i++) {
                int _x_20 = i;
                int _r1 = _x_20;
                for (int _i2 = 0; _i2 < _x_20; _i2++) 
                        _r1 *= _r1;
                printf("%i\n",_r1);
        }
        for (int i = 0; i < 6; i++) 
                printf("%i ",results[i]);
        printf("%i",r);
}

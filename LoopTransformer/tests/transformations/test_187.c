#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5;
	int c[2] = {2, 3};
	#pragma x propagate(ptc, c)
	for (unsigned i = 0; i < 4; i++) {
		int *ptc = c;
		ptc[i] = ptc[i+1] + 1;
		for (unsigned j = 0; j < 4; j++) {
			t = j;
			a[i][ptc[1]] = (i ? 3 : ptc[0]) + c[1] + t/2;
		}
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

